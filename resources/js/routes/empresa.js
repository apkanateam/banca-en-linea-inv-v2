import Vue from 'vue';

import EmpresaHeader from './../components/empresa/_layout/Header';
import EmpresaFooter from './../components/empresa/_layout/Footer';
import Tabla from './../components/tablas/Tabla';
import TablaTransaccionEmpresa from './../components/tablas/TablaTransaccionEmpresa';
import TablaTransaccionesProveedores from './../components/tablas/TablaTransaccionesProveedores';
import TablaSolicitudesChequerasEmpresa from './../components/tablas/TablaSolicitudesChequerasEmpresa';
import TablaSolicitudesChequesDeCajaEmpresa from './../components/tablas/TablaSolicitudesChequesDeCajaEmpresa';
import TablaSelectable from './../components/tablas/TablaSelectable';
import TablaSelectable2 from './../components/tablas/TablaSelectable2';
import TablaBloquearCuenta from './../components/tablas/TablaBloquearCuenta';
import TablaDivisasEmpresa from './../components/tablas/TablaDivisasEmpresa';
import TablaPagosPlanillas from './../components/tablas/TablaPagosPlanillas';
import TablaSeguimientoPlanillas from './../components/tablas/TablaSeguimientoPlanillas';
import TablaEmpleados from './../components/tablas/TablaEmpleados';
import TablaPagosProveedores from './../components/tablas/TablaPagosProveedores';
import SolicitudesIconsEmpresa from './../components/navbar/SolicitudesEmpresa';
import IconChequera from './../components/icons/SolicitarChequera';
import IconCheque from './../components/icons/SolicitarChequeCaja';

Vue.component('empresa-header', EmpresaHeader);
Vue.component('empresa-footer', EmpresaFooter);
Vue.component('table-inv', Tabla);
Vue.component('table-transacciones-inv', TablaTransaccionEmpresa);
Vue.component('table-transacciones-proveedores-inv', TablaTransaccionesProveedores);
Vue.component('table-solicitudes-chequeras-inv', TablaSolicitudesChequerasEmpresa);
Vue.component('table-solicitudes-cheques-inv', TablaSolicitudesChequesDeCajaEmpresa);
Vue.component('table-selectable-inv', TablaSelectable);
Vue.component('table-selectable-2-inv', TablaSelectable2);
Vue.component('table-bloquear-cuenta-inv', TablaBloquearCuenta);
Vue.component('table-divisas-empresa-inv', TablaDivisasEmpresa);
Vue.component('table-planillas', TablaPagosPlanillas);
Vue.component('table-seguimiento-planillas', TablaSeguimientoPlanillas);
Vue.component('table-empleados', TablaEmpleados);
Vue.component('table-proveedores', TablaPagosProveedores);
Vue.component('icon-chequera', IconChequera);
Vue.component('icon-cheque', IconCheque);
Vue.component('solicitudes-icons-empresa', SolicitudesIconsEmpresa);

/* GENERAL */
import EmpresaHome from './../views/empresa/home/Home';
import TransaccionRealizada from './../components/mensajes/TransaccionRealizada';
import TransaccionConfirmar from './../components/mensajes/TransaccionConfirmar';
import TransaccionProveedoresRealizada from './../components/mensajes/TransaccionProveedoresRealizada';
import TransaccionProveedoresConfirmar from './../components/mensajes/TransaccionProveedoresConfirmar';

/* INICIO */
import EmpresaInicio from './../views/empresa/home/Inicio';

/* PERFIL */
import EmpresaPerfilInformacion from './../views/empresa/perfil/informacion/Informacion';
import EmpresaPerfilImagenYFrase from './../views/empresa/perfil/imagen-y-frase/ImagenYFrase';
import EmpresaPerfilContrasenas from './../views/empresa/perfil/contrasenas/Contrasenas';
import EmpresaPerfilPreguntas from './../views/empresa/perfil/preguntas/Preguntas';

/* MIS CUENTAS */
import EmpresaMisCuentasConsulta from './../views/empresa/mis-cuentas/consulta/Consulta';
import EmpresaMisCuentasConsultaEstadosDeCuenta from './../views/empresa/mis-cuentas/consulta/EstadosDeCuenta';
import EmpresaMisCuentasGestiones from './../views/empresa/mis-cuentas/gestiones/Gestiones';

/* TRANSFERENCIAS */
import EmpresaTransferenciasCuentasPropias from './../views/empresa/transferencias/cuentas-propias/CuentasPropias';
import EmpresaTransferenciasCuentaDeTerceros from './../views/empresa/transferencias/cuenta-de-terceros/CuentaDeTerceros';
import EmpresaTransferenciasCuentaDeTercerosAgregar from './../views/empresa/transferencias/cuenta-de-terceros/CuentaDeTercerosAgregar';
import EmpresaTransferenciasCuentaDeTercerosTransferir from './../views/empresa/transferencias/cuenta-de-terceros/CuentaDeTercerosTransferir';
import EmpresaTransferenciasCuentaDeTercerosEditar from './../views/empresa/transferencias/cuenta-de-terceros/CuentaDeTercerosEditar';
import EmpresaTransferenciasOtrosBancos from './../views/empresa/transferencias/otros-bancos/OtrosBancos';
import EmpresaTransferenciasOtrosBancosAgregar from './../views/empresa/transferencias/otros-bancos/OtrosBancosAgregar';
import EmpresaTransferenciasOtrosBancosTransferir from './../views/empresa/transferencias/otros-bancos/OtrosBancosTransferir';
import EmpresaTransferenciasOtrosBancosEditar from './../views/empresa/transferencias/otros-bancos/OtrosBancosEditar';

/* PAGOS */
import EmpresaPagosFavoritos from './../views/empresa/pagos/favoritos/Favoritos';
import EmpresaPagosPrestamos from './../views/empresa/pagos/prestamos/Prestamos';
import EmpresaPagosPrestamosDetalle from './../views/empresa/pagos/prestamos/PrestamosDetalle';
import EmpresaPagosServicios from './../views/empresa/pagos/servicios/Servicios';
import EmpresaPagosServiciosAgregar from './../views/empresa/pagos/servicios/ServiciosAgregar';
import EmpresaPagosServiciosPagar from './../views/empresa/pagos/servicios/ServiciosPagar';
import EmpresaPagosPlanillas from './../views/empresa/pagos/planillas/Planillas';
import EmpresaPagosPlanillasAgregar from './../views/empresa/pagos/planillas/PlanillasAgregar';
import EmpresaPagosPlanillasEditar from './../views/empresa/pagos/planillas/PlanillasEditar';
import EmpresaPagosPlanillasDetalle from './../views/empresa/pagos/planillas/PlanillasDetalle';
import EmpresaPagosPlanillasHistorial from './../views/empresa/pagos/planillas/PlanillasHistorial';
import EmpresaPagosPlanillasEmpleado from './../views/empresa/pagos/planillas/PlanillasEmpleado';
import EmpresaPagosPlanillasPagosEmpleados from './../views/empresa/pagos/planillas/PlanillasPagosEmpleados';
import EmpresaPagosPlanillasPagosEmpleadosDetalle from './../views/empresa/pagos/planillas/PlanillasPagosEmpleadosDetalle';
import EmpresaPagosPlanillasEjecutar from './../views/empresa/pagos/planillas/PlanillasEjecutar';
import EmpresaPagosProveedores from './../views/empresa/pagos/proveedores/Proveedores';
import EmpresaPagosProveedoresAgregar from './../views/empresa/pagos/proveedores/ProveedoresAgregar';
import EmpresaPagosProveedoresPagar from './../views/empresa/pagos/proveedores/ProveedoresPagar';

/* DIVISAS */
import EmpresaDivisas from './../views/empresa/divisas/Divisas';

/* FINANCIAMIENTO */
import EmpresaFinanciamientoSolicitar from './../views/empresa/financiamiento/solicitar/Solicitar';
import EmpresaFinanciamientoPrestamos from './../views/empresa/financiamiento/prestamos/Prestamos';

/* FACTORAJE */
import EmpresaFactoraje from './../views/empresa/factoraje/Factoraje';

/* SOLICITAR */
import EmpresaSolicitarSolicitudesSolicitarChequera from './../views/empresa/solicitar/solicitudes/SolicitarChequera';
import EmpresaSolicitarSolicitudesSolicitarChequeDeCaja from './../views/empresa/solicitar/solicitudes/SolicitarChequeDeCaja';
import EmpresaSolicitarRegistroPortalSIB from './../views/empresa/solicitar/registro-portal-sib/RegistroPortalSIB';

export const empresaRoutes = () => {
  return [
    {
      path: '/empresa',
      name: 'EmpresaHome',
      component: EmpresaHome,
      meta: {
        requiresAuth: true
      },
      children: [
        /* HOME */
        { path: '/empresa', redirect: '/empresa/inicio' },
        { 
          path: '/empresa/inicio', 
          name: 'EmpresaInicio',
          component: EmpresaInicio
        },
        /* PERFIL */
        { path: '/empresa/perfil', redirect: '/empresa/perfil/informacion' },
        { 
          path: '/empresa/perfil/informacion', 
          name: 'EmpresaPerfilInformacion',
          component: EmpresaPerfilInformacion
        },
        { 
          path: '/empresa/perfil/informacion/actualizar/confirmar', 
          name: 'EmpresaPerfilInformacionActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/perfil/informacion/actualizar/realizada', 
          name: 'EmpresaPerfilInformacionActualizarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/perfil/imagen-y-frase', 
          name: 'EmpresaPerfilImagenYFrase',
          component: EmpresaPerfilImagenYFrase
        },
        { 
          path: '/empresa/perfil/imagen-y-frase/actualizar/confirmar', 
          name: 'EmpresaPerfilImagenYFraseActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/perfil/imagen-y-frase/actualizar/realizada', 
          name: 'EmpresaPerfilImagenYFraseActualizarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/perfil/contrasenas', 
          name: 'EmpresaPerfilContrasenas',
          component: EmpresaPerfilContrasenas
        },
        { 
          path: '/empresa/perfil/contrasenas/actualizar/confirmar', 
          name: 'EmpresaPerfilContrasenasActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/perfil/contrasenas/actualizar/realizada', 
          name: 'EmpresaPerfilContrasenasActualizarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/perfil/preguntas', 
          name: 'EmpresaPerfilPreguntas',
          component: EmpresaPerfilPreguntas
        },
        { 
          path: '/empresa/perfil/preguntas/actualizar/confirmar', 
          name: 'EmpresaPerfilPreguntasActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/perfil/preguntas/actualizar/realizada', 
          name: 'EmpresaPerfilPreguntasActualizarRealizada',
          component: TransaccionRealizada
        },
        /* MIS CUENTAS */
        { path: '/empresa/mis-cuentas', redirect: '/empresa/mis-cuentas/consulta' },
        { 
          path: '/empresa/mis-cuentas/consulta', 
          name: 'EmpresaMisCuentasConsulta',
          component: EmpresaMisCuentasConsulta
        },
        { 
          path: '/empresa/mis-cuentas/consulta/estados-de-cuenta', 
          name: 'EmpresaMisCuentasConsultaEstadosDeCuenta',
          component: EmpresaMisCuentasConsultaEstadosDeCuenta
        },
        { 
          path: '/empresa/mis-cuentas/gestiones', 
          name: 'EmpresaMisCuentasGestiones',
          component: EmpresaMisCuentasGestiones
        },
        { 
          path: '/empresa/mis-cuentas/gestiones/bloquear-cuenta/confirmar', 
          name: 'EmpresaMisCuentasGestionesBloquearCuentaConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/mis-cuentas/gestiones/bloquear-cheque/confirmar', 
          name: 'EmpresaMisCuentasGestionesBloquearChequeConfirmar',
          component: TransaccionConfirmar
        },
        /* TRANSFERENCIAS */
        { path: '/empresa/transferencias', redirect: '/empresa/transferencias/cuentas-propias' },
        { 
          path: '/empresa/transferencias/cuentas-propias', 
          name: 'EmpresaTransferenciasCuentasPropias',
          component: EmpresaTransferenciasCuentasPropias,
        },
        { 
          path: '/empresa/transferencias/cuentas-propias/confirmar', 
          name: 'EmpresaTransferenciasCuentasPropiasConfirmar',
          component: TransaccionConfirmar,
        },
        { 
          path: '/empresa/transferencias/cuentas-propias/realizada', 
          name: 'EmpresaTransferenciasCuentasPropiasRealizada',
          component: TransaccionRealizada,
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros', 
          name: 'EmpresaTransferenciasCuentaDeTerceros',
          component: EmpresaTransferenciasCuentaDeTerceros
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/agregar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosAgregar',
          component: EmpresaTransferenciasCuentaDeTercerosAgregar
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/agregar/confirmar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/agregar/realizada', 
          name: 'EmpresaTransferenciasCuentaDeTercerosAgregarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/transferir', 
          name: 'EmpresaTransferenciasCuentaDeTercerosTransferir',
          component: EmpresaTransferenciasCuentaDeTercerosTransferir
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/transferir/confirmar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosTransferirConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/transferir/realizada',
          component: TransaccionRealizada,
          name: 'EmpresaTransferenciasCuentaDeTercerosTransferirRealizada'
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/editar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosEditar',
          component: EmpresaTransferenciasCuentaDeTercerosEditar
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/editar/confirmar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosEditarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/editar/realizada', 
          name: 'EmpresaTransferenciasCuentaDeTercerosEditarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/eliminar/confirmar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosEliminarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/eliminar/realizada', 
          name: 'EmpresaTransferenciasCuentaDeTercerosEliminarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/confirmar', 
          name: 'EmpresaTransferenciasCuentaDeTercerosConfirmar',
          component: TransaccionConfirmar,
        },
        { 
          path: '/empresa/transferencias/cuenta-de-terceros/realizada', 
          name: 'EmpresaTransferenciasCuentaDeTercerosRealizada',
          component: TransaccionRealizada,
        },
        { 
          path: '/empresa/transferencias/otros-bancos', 
          name: 'EmpresaTransferenciasOtrosBancos',
          component: EmpresaTransferenciasOtrosBancos
        },
        { 
          path: '/empresa/transferencias/otros-bancos/agregar', 
          name: 'EmpresaTransferenciasOtrosBancosAgregar',
          component: EmpresaTransferenciasOtrosBancosAgregar
        },
        { 
          path: '/empresa/transferencias/otros-bancos/agregar/confirmar', 
          name: 'EmpresaTransferenciasOtrosBancosAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/otros-bancos/agregar/realizada', 
          name: 'EmpresaTransferenciasOtrosBancosAgregarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/otros-bancos/transferir', 
          name: 'EmpresaTransferenciasOtrosBancosTransferir',
          component: EmpresaTransferenciasOtrosBancosTransferir
        },
        { 
          path: '/empresa/transferencias/otros-bancos/transferir/confirmar', 
          name: 'EmpresaTransferenciasOtrosBancosTransferirConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/otros-bancos/transferir/realizada', 
          name: 'EmpresaTransferenciasOtrosBancosTransferirRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/otros-bancos/editar', 
          name: 'EmpresaTransferenciasOtrosBancosEditar',
          component: EmpresaTransferenciasOtrosBancosEditar
        },
        { 
          path: '/empresa/transferencias/otros-bancos/editar/confirmar', 
          name: 'EmpresaTransferenciasOtrosBancosEditarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/otros-bancos/editar/realizada', 
          name: 'EmpresaTransferenciasOtrosBancosEditarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/otros-bancos/eliminar/confirmar', 
          name: 'EmpresaTransferenciasOtrosBancosEliminarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/transferencias/otros-bancos/eliminar/realizada', 
          name: 'EmpresaTransferenciasOtrosBancosEliminarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/transferencias/otros-bancos/confirmar', 
          name: 'EmpresaTransferenciasOtrosBancosConfirmar',
          component: TransaccionConfirmar,
        },
        { 
          path: '/empresa/transferencias/otros-bancos/realizada', 
          name: 'EmpresaTransferenciasOtrosBancosRealizada',
          component: TransaccionRealizada,
        },
        /* PAGOS */
        { path: '/empresa/pagos', redirect: '/empresa/pagos/favoritos' },
        { 
          path: '/empresa/pagos/favoritos', 
          name: 'EmpresaPagosFavoritos',
          component: EmpresaPagosFavoritos
        },
        { 
          path: '/empresa/pagos/prestamos', 
          name: 'EmpresaPagosPrestamos',
          component: EmpresaPagosPrestamos
        },
        { 
          path: '/empresa/pagos/prestamos/pagar/confirmar', 
          name: 'EmpresaPagosPrestamosPagarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/prestamos/pagar/realizada', 
          name: 'EmpresaPagosPrestamosPagarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/pagos/prestamos/detalle', 
          name: 'EmpresaPagosPrestamosDetalle',
          component: EmpresaPagosPrestamosDetalle
        },
        { 
          path: '/empresa/pagos/prestamos/programar-pago/confirmar', 
          name: 'EmpresaPagosPrestamosProgramarPagoConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/prestamos/programar-pago/realizada', 
          name: 'EmpresaPagosPrestamosProgramarPagoRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/empresa/pagos/servicios', 
          name: 'EmpresaPagosServicios',
          component: EmpresaPagosServicios
        },
        { 
          path: '/empresa/pagos/servicios/agregar', 
          name: 'EmpresaPagosServiciosAgregar',
          component: EmpresaPagosServiciosAgregar
        },
        { 
          path: '/empresa/pagos/servicios/agregar/confirmar', 
          name: 'EmpresaPagosServiciosAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/servicios/agregar/realizada', 
          name: 'EmpresaPagosServiciosAgregarRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/empresa/pagos/servicios/pagar', 
          name: 'EmpresaPagosServiciosPagar',
          component: EmpresaPagosServiciosPagar
        },
        { 
          path: '/empresa/pagos/servicios/pagar/confirmar', 
          name: 'EmpresaPagosServiciosPagarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/servicios/pagar/realizada', 
          name: 'EmpresaPagosServiciosPagarRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/empresa/pagos/planillas', 
          name: 'EmpresaPagosPlanillas',
          component: EmpresaPagosPlanillas
        },
        { 
          path: '/empresa/pagos/planillas/agregar', 
          name: 'EmpresaPagosPlanillasAgregar',
          component: EmpresaPagosPlanillasAgregar
        },
        { 
          path: '/empresa/pagos/planillas/editar/:id', 
          name: 'EmpresaPagosPlanillasEditar',
          component: EmpresaPagosPlanillasEditar
        },
        { 
          path: '/empresa/pagos/planillas/editar/:id/confirmar', 
          name: 'EmpresaPagosPlanillasEditarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/planillas/editar/:id/realizada', 
          name: 'EmpresaPagosPlanillasEditarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/pagos/planillas/detalle/:id', 
          name: 'EmpresaPagosPlanillasDetalle',
          component: EmpresaPagosPlanillasDetalle
        },
        { 
          path: '/empresa/pagos/planillas/historial/:id', 
          name: 'EmpresaPagosPlanillasHistorial',
          component: EmpresaPagosPlanillasHistorial
        },
        { 
          path: '/empresa/pagos/planillas/empleado/:id', 
          name: 'EmpresaPagosPlanillasEmpleado',
          component: EmpresaPagosPlanillasEmpleado
        },
        { 
          path: '/empresa/pagos/planillas/empleado/editar/confirmar', 
          name: 'EmpresaPagosPlanillasEmpleadoEditarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/planillas/empleado/editar/realizada', 
          name: 'EmpresaPagosPlanillasEmpleadoEditarRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/empresa/pagos/planillas/empleados/:id', 
          name: 'EmpresaPagosPlanillasEmpleados',
          component: EmpresaPagosPlanillasPagosEmpleados
        },
        { 
          path: '/empresa/pagos/planillas/empleados/detalle/:id', 
          name: 'EmpresaPagosPlanillasEmpleadosDetalle',
          component: EmpresaPagosPlanillasPagosEmpleadosDetalle
        },
        { 
          path: '/empresa/pagos/planillas/:transaction/:id', 
          name: 'EmpresaPagosPlanillasEjecutar',
          component: EmpresaPagosPlanillasEjecutar
        },
        { 
          path: '/empresa/pagos/proveedores', 
          name: 'EmpresaPagosProveedores',
          component: EmpresaPagosProveedores
        },
        { 
          path: '/empresa/pagos/proveedores/confirmar', 
          name: 'EmpresaPagosProveedoresConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/proveedores/realizada', 
          name: 'EmpresaPagosProveedoresRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/pagos/proveedores/agregar', 
          name: 'EmpresaPagosProveedoresAgregar',
          component: EmpresaPagosProveedoresAgregar
        },
        { 
          path: '/empresa/pagos/proveedores/agregar/confirmar', 
          name: 'EmpresaPagosProveedoresAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/pagos/proveedores/agregar/realizada', 
          name: 'EmpresaPagosProveedoresAgregarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/pagos/proveedores/pagar/:id', 
          name: 'EmpresaPagosProveedoresPagar',
          component: EmpresaPagosProveedoresPagar
        },
        { 
          path: '/empresa/pagos/proveedores/pagar-confirmar', 
          name: 'EmpresaPagosProveedoresPagarConfirmar',
          component: TransaccionProveedoresConfirmar
        },
        { 
          path: '/empresa/pagos/proveedores/pagar-realizada', 
          name: 'EmpresaPagosProveedoresPagarRealizada',
          component: TransaccionProveedoresRealizada
        },
        /* DIVISAS */
        { 
          path: '/empresa/divisas', 
          name: 'EmpresaDivisas',
          component: EmpresaDivisas
        },
        { 
          path: '/empresa/divisas/cambio/confirmar', 
          name: 'EmpresaDivisasCambioConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/divisas/cambio/realizada', 
          name: 'EmpresaDivisasCambioRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/divisas/confirmar', 
          name: 'EmpresaDivisasConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/divisas/realizada', 
          name: 'EmpresaDivisasRealizada',
          component: TransaccionRealizada
        },
        /* FINANCIAMIENTO */
        { path: '/empresa/financiamiento', redirect: '/empresa/financiamiento/solicitar' },
        { 
          path: '/empresa/financiamiento/solicitar', 
          name: 'EmpresaFinanciamientoSolicitar',
          component: EmpresaFinanciamientoSolicitar
        },
        {
          path: '/empresa/financiamiento/solicitar/realizada',
          name: 'EmpresaFinanciamientoSolicitarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/financiamiento/prestamos', 
          name: 'EmpresaFinanciamientoPrestamos',
          component: EmpresaFinanciamientoPrestamos
        },
        /* FACTORAJE */
        { 
          path: '/empresa/factoraje', 
          name: 'EmpresaFactoraje',
          component: EmpresaFactoraje
        },
        { 
          path: '/empresa/factoraje/confirmar', 
          name: 'EmpresaFactorajeConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/factoraje/realizada', 
          name: 'EmpresaFactorajeRealizada',
          component: TransaccionRealizada
        },
        /* SOLICITAR */
        { path: '/empresa/solicitar', redirect: '/empresa/solicitar/solicitudes/solicitar-chequera' },
        { path: '/empresa/solicitar/solicitudes', redirect: '/empresa/solicitar/solicitudes/solicitar-chequera' },
        { 
          path: '/empresa/solicitar/solicitudes/solicitar-chequera', 
          name: 'EmpresaSolicitarSolicitudesSolicitarChequera',
          component: EmpresaSolicitarSolicitudesSolicitarChequera
        },
        { 
          path: '/empresa/solicitar/solicitudes/solicitar-chequera/confirmar', 
          name: 'EmpresaSolicitarSolicitudesSolicitarChequeraConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/solicitar/solicitudes/solicitar-chequera/realizada', 
          name: 'EmpresaSolicitarSolicitudesSolicitarChequeraRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/solicitar/solicitudes/solicitar-cheque-de-caja', 
          name: 'EmpresaSolicitarSolicitudesSolicitarChequeDeCaja',
          component: EmpresaSolicitarSolicitudesSolicitarChequeDeCaja
        },
        { 
          path: '/empresa/solicitar/solicitudes/solicitar-cheque-de-caja/confirmar', 
          name: 'EmpresaSolicitarSolicitudesSolicitarChequeDeCajaConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/empresa/solicitar/solicitudes/solicitar-cheque-de-caja/realizada', 
          name: 'EmpresaSolicitarSolicitudesSolicitarChequeDeCajaRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/empresa/solicitar/registro-portal-sib', 
          name: 'EmpresaSolicitarRegistroPortalSIB',
          component: EmpresaSolicitarRegistroPortalSIB
        },
        { 
          path: '/empresa/solicitar/registro-portal-sib/confirmar', 
          name: 'EmpresaSolicitarRegistroPortalSIBConfirmar',
          component: TransaccionRealizada
        },
      ]
    },    
  ];
}