import Vue from 'vue';

import PersonaHeader from './../components/persona/_layout/Header';
import PersonaFooter from './../components/persona/_layout/Footer';
import Tabla from './../components/tablas/Tabla';
import TablaEdoCuenta from './../components/tablas/TablaEdoCuenta';
import TablaSelectable from './../components/tablas/TablaSelectable';
import TablaBloquearCuenta from './../components/tablas/TablaBloquearCuenta';
import TablaDivisas from './../components/tablas/TablaDivisas';

import SolicitudesIcons from './../components/navbar/Solicitudes';
import IconChequera from './../components/icons/SolicitarChequera';
import IconTarjeta from './../components/icons/SolicitarTarjetaDebito';
import IconCheque from './../components/icons/SolicitarChequeCaja';
import Modal from './../components/modal/Modal';

Vue.component('persona-header', PersonaHeader);
Vue.component('persona-footer', PersonaFooter);
Vue.component('table-inv', Tabla);
Vue.component('table-edo-cuenta-inv', TablaEdoCuenta);
Vue.component('table-selectable-inv', TablaSelectable);
Vue.component('table-bloquear-cuenta-inv', TablaBloquearCuenta);
Vue.component('table-divisas-inv', TablaDivisas);
Vue.component('icon-chequera', IconChequera);
Vue.component('icon-tarjeta', IconTarjeta);
Vue.component('icon-cheque', IconCheque);
Vue.component('solicitudes-icons', SolicitudesIcons);
Vue.component('modal', Modal);
/* GENERAL */
import PersonaHome from './../views/persona/home/Home';
import TransaccionRealizada from './../components/mensajes/TransaccionRealizada';
import TransaccionConfirmar from './../components/mensajes/TransaccionConfirmar';

/* INICIO */
import PersonaInicio from './../views/persona/home/Inicio';

/* PERFIL */
import PersonaPerfilInformacion from './../views/persona/perfil/informacion/Informacion';
import PersonaPerfilImagenYFrase from './../views/persona/perfil/imagen-y-frase/ImagenYFrase';
import PersonaPerfilContrasenas from './../views/persona/perfil/contrasenas/Contrasenas';
import PersonaPerfilPreguntas from './../views/persona/perfil/preguntas/Preguntas';

/* MIS CUENTAS */
import PersonaMisCuentasConsulta from './../views/persona/mis-cuentas/consulta/Consulta';
import PersonaMisCuentasConsultaEstadosDeCuenta from './../views/persona/mis-cuentas/consulta/EstadosDeCuenta';
import PersonaMisCuentasGestiones from './../views/persona/mis-cuentas/gestiones/Gestiones';

/* TRANSFERENCIAS */
import PersonaTransferenciasCuentasPropias from './../views/persona/transferencias/cuentas-propias/CuentasPropias';
import PersonaTransferenciasCuentaDeTerceros from './../views/persona/transferencias/cuenta-de-terceros/CuentaDeTerceros';
import PersonaTransferenciasCuentaDeTercerosAgregar from './../views/persona/transferencias/cuenta-de-terceros/CuentaDeTercerosAgregar';
import PersonaTransferenciasCuentaDeTercerosTransferir from './../views/persona/transferencias/cuenta-de-terceros/CuentaDeTercerosTransferir';
import PersonaTransferenciasCuentaDeTercerosEditar from './../views/persona/transferencias/cuenta-de-terceros/CuentaDeTercerosEditar';
import PersonaTransferenciasOtrosBancos from './../views/persona/transferencias/otros-bancos/OtrosBancos';
import PersonaTransferenciasOtrosBancosAgregar from './../views/persona/transferencias/otros-bancos/OtrosBancosAgregar';
import PersonaTransferenciasOtrosBancosTransferir from './../views/persona/transferencias/otros-bancos/OtrosBancosTransferir';
import PersonaTransferenciasOtrosBancosEditar from './../views/persona/transferencias/otros-bancos/OtrosBancosEditar';
/* PAGOS */
import PersonaPagosFavoritos from './../views/persona/pagos/favoritos/Favoritos';
import PersonaPagosPrestamos from './../views/persona/pagos/prestamos/Prestamos';
import PersonaPagosPrestamosDetalle from './../views/persona/pagos/prestamos/PrestamosDetalle';
import PersonaPagosServicios from './../views/persona/pagos/servicios/Servicios';
import PersonaPagosServiciosAgregar from './../views/persona/pagos/servicios/ServiciosAgregar';
import PersonaPagosServiciosPagar from './../views/persona/pagos/servicios/ServiciosPagar';

/* DIVISAS */
import PersonaDivisas from './../views/persona/divisas/Divisas';

/* FINANCIAMIENTO */
import PersonaFinanciamientoSolicitar from './../views/persona/financiamiento/solicitar/Solicitar';
import PersonaFinanciamientoPrestamos from './../views/persona/financiamiento/prestamos/Prestamos';

/* SOLICITAR */
import PersonaSolicitarSolicitudesSolicitarChequera from './../views/persona/solicitar/solicitudes/SolicitarChequera';
import PersonaSolicitarSolicitudesSolicitarTarjetaDeDebito from './../views/persona/solicitar/solicitudes/SolicitarTarjetaDeDebito';
import PersonaSolicitarSolicitudesSolicitarChequeDeCaja from './../views/persona/solicitar/solicitudes/SolicitarChequeDeCaja';
import PersonaSolicitarRegistroPortalSIB from './../views/persona/solicitar/registro-portal-sib/RegistroPortalSIB';

export const personaRoutes = () => {
  return [
    {
      path: '/persona',
      name: 'PersonaHome',
      component: PersonaHome,
      meta: {
        requiresAuth: true
      },
      children: [
        /* HOME */
        { path: '/persona', redirect: '/persona/inicio' },
        { 
          path: '/persona/inicio', 
          name: 'PersonaInicio',
          component: PersonaInicio
        },
        /* PERFIL */
        { path: '/persona/perfil', redirect: '/persona/perfil/informacion' },
        { 
          path: '/persona/perfil/informacion', 
          name: 'PersonaPerfilInformacion',
          component: PersonaPerfilInformacion
        },
        { 
          path: '/persona/perfil/informacion/actualizar/confirmar', 
          name: 'PersonaPerfilInformacionActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/perfil/informacion/actualizar/realizada', 
          name: 'PersonaPerfilInformacionActualizarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/perfil/imagen-y-frase', 
          name: 'PersonaPerfilImagenYFrase',
          component: PersonaPerfilImagenYFrase
        },
        { 
          path: '/persona/perfil/imagen-y-frase/actualizar/confirmar', 
          name: 'PersonaPerfilImagenYFraseActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/perfil/imagen-y-frase/actualizar/realizada', 
          name: 'PersonaPerfilImagenYFraseActualizarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/perfil/contrasenas', 
          name: 'PersonaPerfilContrasenas',
          component: PersonaPerfilContrasenas
        },
        { 
          path: '/persona/perfil/contrasenas/actualizar/confirmar', 
          name: 'PersonaPerfilContrasenasActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/perfil/contrasenas/actualizar/realizada', 
          name: 'PersonaPerfilContrasenasActualizarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/perfil/preguntas', 
          name: 'PersonaPerfilPreguntas',
          component: PersonaPerfilPreguntas
        },
        { 
          path: '/persona/perfil/preguntas/actualizar/confirmar', 
          name: 'PersonaPerfilPreguntasActualizarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/perfil/preguntas/actualizar/realizada', 
          name: 'PersonaPerfilPreguntasActualizarRealizada',
          component: TransaccionRealizada
        },
        /* MIS CUENTAS */
        { path: '/persona/mis-cuentas', redirect: '/persona/mis-cuentas/consulta' },
        { 
          path: '/persona/mis-cuentas/consulta', 
          name: 'PersonaMisCuentasConsulta',
          component: PersonaMisCuentasConsulta
        },
        { 
          path: '/persona/mis-cuentas/consulta/estados-de-cuenta', 
          name: 'PersonaMisCuentasConsultaEstadosDeCuenta',
          component: PersonaMisCuentasConsultaEstadosDeCuenta
        },
        { 
          path: '/persona/mis-cuentas/gestiones', 
          name: 'PersonaMisCuentasGestiones',
          component: PersonaMisCuentasGestiones
        },
        { 
          path: '/persona/mis-cuentas/gestiones/bloquear-cuenta/confirmar', 
          name: 'PersonaMisCuentasGestionesBloquearCuentaConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/mis-cuentas/gestiones/bloquear-cuenta/realizada', 
          name: 'PersonaMisCuentasGestionesBloquearCuentaRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/mis-cuentas/gestiones/bloquear-cheque/confirmar', 
          name: 'PersonaMisCuentasGestionesBloquearChequeConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/mis-cuentas/gestiones/bloquear-cheque/realizada', 
          name: 'PersonaMisCuentasGestionesBloquearChequeRealizada',
          component: TransaccionRealizada
        },
        /* TRANSFERENCIAS */
        { path: '/persona/transferencias', redirect: '/persona/transferencias/cuentas-propias' },
        { 
          path: '/persona/transferencias/cuentas-propias', 
          name: 'PersonaTransferenciasCuentasPropias',
          component: PersonaTransferenciasCuentasPropias,
        },
        { 
          path: '/persona/transferencias/cuentas-propias/confirmar', 
          name: 'PersonaTransferenciasCuentasPropiasConfirmar',
          component: TransaccionConfirmar,
        },
        { 
          path: '/persona/transferencias/cuentas-propias/realizada', 
          name: 'PersonaTransferenciasCuentasPropiasRealizada',
          component: TransaccionRealizada,
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros', 
          name: 'PersonaTransferenciasCuentaDeTerceros',
          component: PersonaTransferenciasCuentaDeTerceros
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/agregar', 
          name: 'PersonaTransferenciasCuentaDeTercerosAgregar',
          component: PersonaTransferenciasCuentaDeTercerosAgregar
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/agregar/confirmar', 
          name: 'PersonaTransferenciasCuentaDeTercerosAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/agregar/realizada', 
          name: 'PersonaTransferenciasCuentaDeTercerosAgregarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/transferir', 
          name: 'PersonaTransferenciasCuentaDeTercerosTransferir',
          component: PersonaTransferenciasCuentaDeTercerosTransferir
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/transferir/confirmar', 
          name: 'PersonaTransferenciasCuentaDeTercerosTransferirConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/transferir/realizada',
          component: TransaccionRealizada,
          name: 'PersonaTransferenciasCuentaDeTercerosTransferirRealizada'
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/editar', 
          name: 'PersonaTransferenciasCuentaDeTercerosEditar',
          component: PersonaTransferenciasCuentaDeTercerosEditar
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/editar/confirmar', 
          name: 'PersonaTransferenciasCuentaDeTercerosEditarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/editar/realizada', 
          name: 'PersonaTransferenciasCuentaDeTercerosEditarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/eliminar/confirmar', 
          name: 'PersonaTransferenciasCuentaDeTercerosEliminarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/cuenta-de-terceros/eliminar/realizada', 
          name: 'PersonaTransferenciasCuentaDeTercerosEliminarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/transferencias/otros-bancos', 
          name: 'PersonaTransferenciasOtrosBancos',
          component: PersonaTransferenciasOtrosBancos
        },
        { 
          path: '/persona/transferencias/otros-bancos/agregar', 
          name: 'PersonaTransferenciasOtrosBancosAgregar',
          component: PersonaTransferenciasOtrosBancosAgregar
        },
        { 
          path: '/persona/transferencias/otros-bancos/agregar/confirmar', 
          name: 'PersonaTransferenciasOtrosBancosAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/otros-bancos/agregar/realizada', 
          name: 'PersonaTransferenciasOtrosBancosAgregarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/transferencias/otros-bancos/transferir', 
          name: 'PersonaTransferenciasOtrosBancosTransferir',
          component: PersonaTransferenciasOtrosBancosTransferir
        },
        { 
          path: '/persona/transferencias/otros-bancos/transferir/confirmar', 
          name: 'PersonaTransferenciasOtrosBancosTransferirConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/otros-bancos/transferir/realizada', 
          name: 'PersonaTransferenciasOtrosBancosTransferirRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/transferencias/otros-bancos/editar', 
          name: 'PersonaTransferenciasOtrosBancosEditar',
          component: PersonaTransferenciasOtrosBancosEditar
        },
        { 
          path: '/persona/transferencias/otros-bancos/editar/confirmar', 
          name: 'PersonaTransferenciasOtrosBancosEditarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/otros-bancos/editar/realizada', 
          name: 'PersonaTransferenciasOtrosBancosEditarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/transferencias/otros-bancos/eliminar/confirmar', 
          name: 'PersonaTransferenciasOtrosBancosEliminarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/transferencias/otros-bancos/eliminar/realizada', 
          name: 'PersonaTransferenciasOtrosBancosEliminarRealizada',
          component: TransaccionRealizada
        },
        /* PAGOS */
        { path: '/persona/pagos', redirect: '/persona/pagos/favoritos' },
        { 
          path: '/persona/pagos/favoritos', 
          name: 'PersonaPagosFavoritos',
          component: PersonaPagosFavoritos
        },
        { 
          path: '/persona/pagos/prestamos', 
          name: 'PersonaPagosPrestamos',
          component: PersonaPagosPrestamos
        },
        { 
          path: '/persona/pagos/prestamos/pagar/confirmar', 
          name: 'PersonaPagosPrestamosPagarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/pagos/prestamos/pagar/realizada', 
          name: 'PersonaPagosPrestamosPagarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/pagos/prestamos/detalle', 
          name: 'PersonaPagosPrestamosDetalle',
          component: PersonaPagosPrestamosDetalle
        },
        { 
          path: '/persona/pagos/prestamos/programar-pago/confirmar', 
          name: 'PersonaPagosPrestamosProgramarPagoConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/pagos/prestamos/programar-pago/realizada', 
          name: 'PersonaPagosPrestamosProgramarPagoRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/persona/pagos/servicios', 
          name: 'PersonaPagosServicios',
          component: PersonaPagosServicios
        },
        { 
          path: '/persona/pagos/servicios/agregar', 
          name: 'PersonaPagosServiciosAgregar',
          component: PersonaPagosServiciosAgregar
        },
        { 
          path: '/persona/pagos/servicios/agregar/confirmar', 
          name: 'PersonaPagosServiciosAgregarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/pagos/servicios/agregar/realizada', 
          name: 'PersonaPagosServiciosAgregarRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/persona/pagos/servicios/pagar', 
          name: 'PersonaPagosServiciosPagar',
          component: PersonaPagosServiciosPagar
        },
        { 
          path: '/persona/pagos/servicios/pagar/confirmar', 
          name: 'PersonaPagosServiciosPagarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/pagos/servicios/pagar/realizada', 
          name: 'PersonaPagosServiciosPagarRealizada',
          component: TransaccionRealizada 
        },
        { 
          path: '/persona/pagos/servicios/eliminar/confirmar', 
          name: 'PersonaPagosServiciosEliminarConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/pagos/servicios/eliminar/realizada', 
          name: 'PersonaPagosServiciosEliminarRealizada',
          component: TransaccionRealizada
        },
        /* DIVISAS */
        { 
          path: '/persona/divisas', 
          name: 'PersonaDivisas',
          component: PersonaDivisas
        },
        { 
          path: '/persona/divisas/cambio/confirmar', 
          name: 'PersonaDivisasCambioConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/divisas/cambio/realizada', 
          name: 'PersonaDivisasCambioRealizada',
          component: TransaccionRealizada
        },
        /* FINANCIAMIENTO */
        { path: '/persona/financiamiento', redirect: '/persona/financiamiento/solicitar' },
        { 
          path: '/persona/financiamiento/solicitar', 
          name: 'PersonaFinanciamientoSolicitar',
          component: PersonaFinanciamientoSolicitar
        },
        {
          path: '/persona/financiamiento/solicitar/confirmar',
          name: 'PersonaFinanciamientoSolicitarConfirmar',
          component: TransaccionConfirmar
        },
        {
          path: '/persona/financiamiento/solicitar/realizada',
          name: 'PersonaFinanciamientoSolicitarRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/financiamiento/prestamos', 
          name: 'PersonaFinanciamientoPrestamos',
          component: PersonaFinanciamientoPrestamos
        },
        /* SOLICITAR */
        { path: '/persona/solicitar', redirect: '/persona/solicitar/solicitudes/solicitar-chequera' },
        { path: '/persona/solicitar/solicitudes', redirect: '/persona/solicitar/solicitudes/solicitar-chequera' },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-chequera', 
          name: 'PersonaSolicitarSolicitudesSolicitarChequera',
          component: PersonaSolicitarSolicitudesSolicitarChequera
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-chequera/confirmar', 
          name: 'PersonaSolicitarSolicitudesSolicitarChequeraConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-chequera/realizada', 
          name: 'PersonaSolicitarSolicitudesSolicitarChequeraRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-tarjeta-de-debito', 
          name: 'PersonaSolicitarSolicitudesSolicitarTarjetaDeDebito',
          component: PersonaSolicitarSolicitudesSolicitarTarjetaDeDebito
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-tarjeta-de-debito/confirmar', 
          name: 'PersonaSolicitarSolicitudesSolicitarTarjetaDeDebitoConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-tarjeta-de-debito/realizada', 
          name: 'PersonaSolicitarSolicitudesSolicitarTarjetaDeDebitoRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-cheque-de-caja', 
          name: 'PersonaSolicitarSolicitudesSolicitarChequeDeCaja',
          component: PersonaSolicitarSolicitudesSolicitarChequeDeCaja
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-cheque-de-caja/confirmar', 
          name: 'PersonaSolicitarSolicitudesSolicitarChequeDeCajaConfirmar',
          component: TransaccionConfirmar
        },
        { 
          path: '/persona/solicitar/solicitudes/solicitar-cheque-de-caja/realizada', 
          name: 'PersonaSolicitarSolicitudesSolicitarChequeDeCajaRealizada',
          component: TransaccionRealizada
        },
        { 
          path: '/persona/solicitar/registro-portal-sib', 
          name: 'PersonaSolicitarRegistroPortalSIB',
          component: PersonaSolicitarRegistroPortalSIB
        },
        { 
          path: '/persona/solicitar/registro-portal-sib/confirmar', 
          name: 'PersonaSolicitarRegistroPortalSIBConfirmar',
          component: TransaccionRealizada
        },
      ]
    },    
  ];
}