import Login from './../views/autenticacion/Login';
import LoginUser from './../views/autenticacion/LoginUser';
import RecuperarContrasenaUser from './../views/autenticacion/RecuperarContrasenaUser';
import EnviarContrasenaSMSEmailUser from './../views/autenticacion/EnviarContrasenaSMSEmailUser';
import ActualizarContrasenaTemporalUser from './../views/autenticacion/ActualizarContrasenaTemporalUser';
import ActualizarContrasenaTemporalS from './../views/autenticacion/ActualizarContrasenaTemporalS';
import NotFound from './../views/NotFound';
export const authRoutes = () => {
  return [
    { path: '/', redirect: '/login' },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/login/user/:profile/:username',
      name: 'LoginUser',
      component: LoginUser
    },
    {
      path: '/recuperar-contrasena/user/:username',
      name: 'RecuperarContrasenaUser',
      component: RecuperarContrasenaUser
    },
    {
      path: '/enviar-contrasena-sms-email/user/:username',
      name: 'EnviarContrasenaSMSEmailUser',
      component: EnviarContrasenaSMSEmailUser
    },
    {
      path: '/actualizar-contrasena-temporal/user/:username',
      name: 'ActualizarContrasenaTemporalUser',
      component: ActualizarContrasenaTemporalUser
    },
    {
      path: '/actualizar-contrasena-temporal-s/user/:username',
      name: 'ActualizarContrasenaTemporalS',
      component: ActualizarContrasenaTemporalS
    },
    { path: '/404', component: NotFound },  
    { path: '*', redirect: '/404' },  
  ];
}