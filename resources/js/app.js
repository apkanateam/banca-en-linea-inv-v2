import 'v-slim-dialog/dist/v-slim-dialog.css'
import './bootstrap';

import Vue from 'vue';
import VueRouter from 'vue-router';
import SlimDialog from 'v-slim-dialog';
import VCalendar from 'v-calendar';
import VueCurrencyFilter from 'vue-currency-filter';
import VueFilterDateFormat from 'vue-filter-date-format';
import VueScrollTo from 'vue-scrollto';
import JsonExcel from 'vue-json-excel';
import VueCarousel from 'vue-carousel';

Vue.use(SlimDialog)
Vue.use(VCalendar);
Vue.use(VueRouter);
Vue.use(VueFilterDateFormat);
Vue.use(VueScrollTo);
Vue.component('downloadExcel', JsonExcel);
Vue.use(VueCarousel);
Vue.use(VueCurrencyFilter,{
	symbol : 'Q.',
	thousandsSeparator: ',',
	fractionCount: 2,
	fractionSeparator: '.',
	symbolPosition: 'front',
	symbolSpacing: false
});

window.slimDialogError = { title: 'Error', size: 'sm', okLabel: 'Cerrar' }

import App from './views/App';
import { authRoutes } from './routes/auth';
import { personaRoutes } from './routes/persona';
import { empresaRoutes } from './routes/empresa';

const routes = authRoutes().concat(personaRoutes().concat(empresaRoutes()))

const router = new VueRouter({
	mode: 'history',
	routes: routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!localStorage.getItem('inv.user')) {
      next({ name: 'Login' })
    } else {
      next() // go to wherever I'm going
    }
  } else {
    next() // does not require auth, make sure to always call next()!
  }
})

Vue.component('app', App);

const app = new Vue({
	el: '#app',
	router
});