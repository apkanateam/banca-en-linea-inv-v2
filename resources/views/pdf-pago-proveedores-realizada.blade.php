<!DOCTYPE html>
<html lang="es">
<head>
  <title>Banco INV</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    @page {
      margin: 5cm 0cm 1.5cm;
      font-family: Arial, sans-serif;
    }
    body {
      font-family: Arial, sans-serif;
      margin: 1cm 0.5cm 1cm;
      color: #555;
      font-size: 13px;
    }
    header {
      position: fixed;
      top: 0;
      left: 0cm;
      right: 0cm;
      background-color: #fff;
      text-align: left;
      margin-top: -2.5cm
    }
    .logo{
      width: 4.5cm
    }
    .title{
      text-align: center;
      background: #f1f1f1;
      text-transform: uppercase;
      padding: 0.15cm 1cm;
      letter-spacing: 1px;
    }
    .text-danger{
      color: #e3342f;
    }
    .text-success{
      color: #45803c;
    }
    .text-uppercase{
      text-transform: uppercase;
    }
    .form-confirmar-exito, .wd-700 {
      width: 14cm;
      margin: 0 auto;
      font-size: 12px;
      padding-left: 1cm;
    }
    .wd-700 {
      padding-left: 0;
    }
    .col-12{
      width: 100%;
    }
    .col-md-6 {
      width: 50%;
      float: left;
    }
    .wd-700 .col-md-6{
      width: 49%;
      float: none;
      display: inline-block;
    }
    .d-block{
      display: block;
    }
    .mb-3, .my-3 {
      margin-bottom: 1rem !important;
    }
    .ml-3, .mx-3 {
      margin-left: 1rem !important;
    }
    .mb-4, .my-4 {
      margin-bottom: 1.5rem !important;
    }
    .lines-2 {
      height: 35px;
    }
    .text-right{
      text-align: right;
    }
    .image{
      width: 100px;
      height: 100px;
      background-position: center;
      background-size: auto 100%;
      border-radius: 5px;
      border: 2px solid #2B401B;
      cursor: pointer;
    }
    .table-cell{
      display: table-cell;
      vertical-align:top;
    }
  </style>
</head>
<body>
  <header class="text-uppercase">
    <h3 class="title">Resumen</h3>
  </header>
  <main>
    @foreach ($info->cols as $key => $cols)
      <h4 style="text-align:center">{{ $info->dataApi[$key]->title }}</h4>
      <p style="text-align:center">{{ $info->dataApi[$key]->message ?? '' }}</p>
      @if ($cols->col1 && $cols->col2)
      <div class="row form-confirmar-exito text-uppercase mb-4">
        <div class="col-md-6">
          @foreach ($cols->col1 as $col)
          <div class="position-relative ml-3 mb-3">
            <div v-if="col.name">
              <b class="d-block">{{ $col->name }}</b>
              <span class="d-block">
                @if (isset($col->currency))
                <span>{{ $col->currency.number_format($col->value, 2, '.', ',') }}</span>
                @else 
                <span>{{ $col->value }}</span>
                @endif
              </span>
            </div>
          </div>
          @endforeach
        </div>
        <div class="col-md-6">
          @foreach ($cols->col2 as $col)
          <div class="position-relative ml-3 mb-3">
            <div>
              <b class="d-block">{{ $col->name }}</b>
              <span class="d-block">
              @if (isset($col->currency))
                <span>{{ $col->currency.number_format($col->value, 2, '.', ',') }}</span>
                @else 
                <span>{{ $col->value }}</span>
                @endif
              </span>
            </div>
          </div>
          @endforeach
        </div>
        <div style="clear: both"></div>
      </div>
      @endif
    @endforeach
  </main>

  <script type="text/php">
    if ( isset($pdf) ) {
      $pdf->page_script('
        $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
        $pdf->text(510, 35, "PÁGINA $PAGE_NUM DE $PAGE_COUNT", $font, 10);
        $pdf->image("images/logo-banco-inv.png", 30, 20, 87, 36);
        $pdf->text(445, 800, "FECHA: ".date("d-m-Y H:i:s"), $font, 10);
        $pdf->text(20, 800, "USUARIO: {{$info->username}}", $font, 10);
        $pdf->text(20, 820, "10 CALLE A 0-79 ZONA 10 CIUDAD DE GUATEMALA",  $font, 8);
        $pdf->text(330, 820, "INV@INV.COM.GT",  $font, 8);
        $pdf->text(517, 820, "(502) 2315-6600",  $font, 8);
      ');
    }
  </script>
</body>
</html>