<!DOCTYPE html>
<html lang="es">
<head>
  <title>Banco INV</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    @page {
      margin: 5cm 0cm 1.5cm;
      font-family: Arial, sans-serif;
    }
    body {
      font-family: Arial, sans-serif;
      margin: 1cm 0.5cm 1cm;
      color: #555;
      font-size: 13px;
    }
    header {
      position: fixed;
      top: 0;
      left: 0cm;
      right: 0cm;
      background-color: #fff;
      text-align: left;
      margin-top: -3cm
    }
    footer {
      position: fixed;
      bottom: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      background-color: #2a0927;
      text-align: center;
      line-height: 35px;
    }
    #resumen{
      letter-spacing: 1px;
      border: 1px solid #eee;
      padding: 0.25cm 0.5cm;
    }
    .logo{
      width: 4.5cm
    }
    .title{
      text-align: center;
      background: #f1f1f1;
      text-transform: uppercase;
      padding: 0.15cm 1cm;
      letter-spacing: 1px;
    }
    #info{
      margin-top: 0.1cm;
      padding: 0.25cm 0.5cm 0;
      text-transform: uppercase;
      line-height: 1.75
    }
    #info label{
      font-weight: bold;
    }
    #info>div{
      width: 33.3333%;
      display: inline-block;
      font-size: 11px;
      letter-spacing: 1px;
    }
    #info .name{
     position: absolute;
     width: 11.25cm;
     height: 0.65cm;
     overflow: hidden;
    }
    table {
      border-collapse: collapse;
      width: 100%;
      font-size: 11px;
      text-transform: uppercase;
    }
    thead{
      background: #518844;
      color: #fff;
      text-transform: uppercase;
      font-size: 12px;
    }
    td, th {
      text-align: left;
      padding: 8px;
    }
    tr:nth-child(even) {
      background-color: #eee;
    }
  </style>
</head>
<body>
  <header>
    <h3 class="title">{{ $title }}</h3>

    <div id="info">
      <div>
        <div>
          <label>Cuenta: </label> {{ $account->number }}
        </div>
        <div>
          <label>Saldo Inicial: </label> {{ $balance }}
        </div>
        <div>
          <label>Fecha Inicial: </label> {{ $start }}
        </div>
      </div>
      <div>
        <div>
          <label>Nombre: </label> <span class="name">{{ $account->name }}</span>
        </div>
        <div>
          <label>Saldo Final: </label>  {{ $currency[$account->currency][0] }}{{ number_format(end($transactions)->balance,2 ) }}
        </div>
        <div>
          <label>Fecha Final: </label> {{ $end }}
        </div>
      </div>
      <div>
        <div>
          <label>Moneda: </label> {{ $currency[$account->currency][1] }}
        </div>
        <div>
          <label>Producto: </label> {{ $account->typeDesc }}
        </div>
      </div>
    </div>
  </header>
  <main>
    
    <table style="max-width: 12cm!important">
      <thead>
        <tr>
          <th style="width: 2.25cm">Fecha</th>
          <th style="width: 1.5cm">Tr</th>
          <th style="width: 5.5cm">Actividad</th>
          <th style="width: 2cm; text-align: right">No. de Doc</th>
          <th style="text-align: right">Debe</th>
          <th style="text-align: right">Haber</th>
          <th style="text-align: right">Saldo</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($transactions as $t)
        <tr>
          <td style="width: 2.25cm">{{ date('d-m-Y', strtotime(substr($t->date, 0, 10))) }}</td>
          <td style="width: 1.5cm">{{ $t->type }}</td>
          <td style="width: 5.5cm">{{ $t->description }}</td>
          <td style="width: 2cm; text-align: right;">{{ strip_tags($t->reference) }}</td>
          <td style="text-align: right">{{ $currency[$account->currency][0] }}{{ number_format($t->debit,2 ) }}</td>
          <td style="text-align: right">{{ $currency[$account->currency][0]}}{{ number_format($t->credit,2)  }}</td>
          <td  style="text-align: right"><b>{{ $currency[$account->currency][0]}}{{ number_format($t->balance,2)  }}</b></td>
          @if ($t->debit > 0) 
           {{ $mov['debito'][0] += 1 }}
           {{ $mov['debito'][1] += $t->debit }}
          @endif

          @if ($t->credit > 0) 
           {{ $mov['credito'][0] += 1 }}
           {{ $mov['credito'][1] += $t->credit }}
          @endif

          @if ($t->type == 'CQ') 
           {{ $mov['cheques'][0] += 1 }}
           {{ $mov['cheques'][1] += $t->debit }}
          @endif
        </tr>
      @endforeach

      </tbody>
      <tfoot>
        <tr>
          <td colspan="7">
            <div id="resumen" style="text-transform: uppercase; text-align: center; margin: 10px auto; max-width: 12cm; font-size: 11px">
              <h3 style="line-height: 2; margin:0">Resumen de movimientos de Mes</h3>
              <div class="movs" style="margin-top: 0.5cm">
                <div style="width: 35%; display: inline-block; text-align: left">
                  <b>Descripción</b>
                  <div>Débitos</div>
                  <div>Créditos</div>
                  <div>Cheques Pagados</div>
                </div>
                <div style="width: 30%; display: inline-block; text-align:center">
                  <b>Cantidad</b>
                  <div>{{ $mov['debito'][0] }}</div>
                  <div>{{ $mov['credito'][0] }}</div>
                  <div>{{ $mov['cheques'][0] }}</div>
                </div>
                <div style="width: 32%; display: inline-block; text-align: right">
                  <b>Monto</b>
                  <div>{{ $currency[$account->currency][0] }}{{ number_format($mov['debito'][1] ,2 ) }} </div>
                  <div>{{ $currency[$account->currency][0] }}{{ number_format($mov['credito'][1] ,2 ) }}</div>
                  <div>{{ $currency[$account->currency][0] }}{{ number_format($mov['cheques'][1] ,2 ) }}</div>
                </div>
              </div>
            </div>
          </td>
        </tr>
      </tfoot>
    </table>
    
  </main>

  <script type="text/php">
    if ( isset($pdf) ) {
      $pdf->page_script('
        $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
        $pdf->text(510, 35, "PÁGINA $PAGE_NUM DE $PAGE_COUNT", $font, 10);
        $pdf->image("images/logo-banco-inv.png", 30, 20, 87, 36);
        $pdf->text(445, 800, "FECHA: ".date("d-m-Y H:i:s"), $font, 10);
        $pdf->text(20, 800, "USUARIO: {{$username}}", $font, 10);
        $pdf->text(20, 820, "10 CALLE A 0-79 ZONA 10 CIUDAD DE GUATEMALA",  $font, 8);
        $pdf->text(330, 820, "INV@INV.COM.GT",  $font, 8);
        $pdf->text(517, 820, "(502) 2315-6600",  $font, 8);
      ');
    }
  </script>
</body>
</html>