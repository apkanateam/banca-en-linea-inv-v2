<!DOCTYPE html>
<html lang="es">
<head>
  <title>Banco INV</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    @page {
      margin: 5cm 0cm 1.5cm;
      font-family: Arial, sans-serif;
    }
    body {
      font-family: Arial, sans-serif;
      margin: 1cm 0.5cm 1cm;
      color: #555;
      font-size: 13px;
    }
    header {
      position: fixed;
      top: 0;
      left: 0cm;
      right: 0cm;
      background-color: #fff;
      text-align: left;
      margin-top: -2.5cm
    }
    .logo{
      width: 4.5cm
    }
    .title{
      text-align: center;
      background: #f1f1f1;
      text-transform: uppercase;
      padding: 0.15cm 1cm;
      letter-spacing: 1px;
    }
    .text-danger{
      color: #e3342f;
    }
    .text-success{
      color: #45803c;
    }
    .text-uppercase{
      text-transform: uppercase;
    }
    .form-confirmar-exito, .wd-700 {
      width: 14cm;
      margin: 0 auto;
      font-size: 12px;
      padding-left: 1cm;
    }
    .wd-700 {
      padding-left: 0;
    }
    .col-12{
      width: 100%;
    }
    .col-md-6 {
      width: 50%;
      float: left;
    }
    .wd-700 .col-md-6{
      width: 49%;
      float: none;
      display: inline-block;
    }
    .d-block{
      display: block;
    }
    .mb-3, .my-3 {
      margin-bottom: 1rem !important;
    }
    .ml-3, .mx-3 {
      margin-left: 1rem !important;
    }
    .mb-4, .my-4 {
      margin-bottom: 1.5rem !important;
    }
    .lines-2 {
      height: 35px;
    }
    .text-right{
      text-align: right;
    }
    .image{
      width: 100px;
      height: 100px;
      background-position: center;
      background-size: auto 100%;
      border-radius: 5px;
      border: 2px solid #2B401B;
      cursor: pointer;
    }
    .table-cell{
      display: table-cell;
      vertical-align:top;
    }
  </style>
</head>
<body>
  <header class="text-uppercase">
    <h3 class="title <?php if($info->success) { echo 'text-success'; } else  { echo 'text-danger'; } ?>">{{ $info->title }}</h3>
    @if ($info->text)
      <p style="text-align: center; padding: 0 10px" class="<?php if($info->success) { echo 'text-success'; } else  { echo 'text-danger'; } ?>">{{ $info->text }}</p>
    @endif
  </header>
  <main>

    @if ($info->col1 && isset($info->col2))
    <div class="row form-confirmar-exito text-uppercase mb-4">
      <div class="col-12">
        @if (isset($info->col1[0]->type) && $info->col1[0]->type == 'image-url')
        <div class="image-url text-center">
            <img src="{{ base_path() }}/public{{ $info->col1[0]->value }}" alt="" width="112" style="margin-left: 190px; ">
        </div>
        @endif
      </div>
      <div class="col-md-6">
        @foreach ($info->col1 as $col)
          @if (isset($col->name))
            <div class="position-relative ml-3 mb-3">
              <b class="d-block">{{ $col->name }}</b>
              <span class="d-block <?php if (isset($col->lines) && $col->lines == 2) { echo 'lines-2'; } ?>">
                @if (isset($col->currency))
                {{ $col->currency.number_format($col->value, 2, '.', ',') }}
                @else 
                  {{ $col->value }}
                @endif
              </span>
            </div>
          @endif
        @endforeach
        @if ( isset($info->resp->success) && isset($info->resp->code))
        <div class="position-relative ml-3 mb-3">
          <b class="d-block">Código de transacción</b>
          <span class="d-block">{{ $info->resp->code }}</span>
        </div>
        @endif
      </div>
      <div class="col-md-6">
        @foreach ($info->col2 as $col)
          @if (isset($col->name))
            <div class="position-relative ml-3 mb-3">
              <b class="d-block">{{ $col->name }}</b>
              <span class="d-block <?php if (isset($col->lines) && $col->lines == 2) { echo 'lines-2'; } ?>">
                @if (isset($col->value))
                  @if (isset($col->currency))
                  {{ $col->currency.number_format($col->value, 2, '.', ',') }}
                  @else 
                    {{ $col->value }}
                  @endif
                @endif
              </span>
            </div>
          @endif
        @endforeach
        @if (isset($info->resp->success) && isset($info->resp->dateTime))
        <div class="position-relative ml-3 mb-3">
          <b class="d-block">Fecha y hora</b>
          <span class="d-block">{{ date('d-m-Y H:i:s', strtotime($info->resp->dateTime)) }}</span>
        </div>
        @endif
      </div>
    </div>
    @endif

    @if ($info->col1 && !isset($info->col2))
      <div class="wd-700 mx-auto text-uppercase mb-4">
        @foreach ($info->col1 as $col)
          <div style="margin-bottom: 12px">
            @if (isset($col->value))
              <div class="col-md-6 text-right table-cell">
                <b style="margin-right: 12px: ">{{ $col->name }}</b>
              </div>
              <div class="col-md-6 table-cell">
                <span class="d-block <?php if (isset($col->lines) && $col->lines == 2) { echo 'lines-2'; } ?>">
                @if (isset($col->currency))
                  {{ $col->currency.number_format($col->value, 2, '.', ',') }}
                @endif
                @if (isset($col->type) && $col->type == 'image')
                  <div class="image" style="background-image: url(data:image.png;base64,{{ $col->value }})"></div>
                @endif
                @if (!isset($col->currency) && !isset($col->type)) 
                  {{ $col->value }}
                @endif
                </span>
              </div>
            @endif
          </div>
        @endforeach
        @if (isset($info->resp->success) && isset($info->resp->code))
          <div class="row form-row mb-1">
            <div class="col-md-6 text-right">
              <b>Código de transacción</b>
            </div>
            <div class="col-md-6 ">
              <span class="d-block">{{ $info->resp->code }}</span>
            </div>
          </div>
        @endif
        @if (isset($info->resp->success) && isset($info->resp->dateTime))
          <div class="row form-row mb-1">
            <div class="col-md-6 text-center text-sm-right">
              <b>Fecha y hora</b>
            </div>
            <div class="col-md-6 ">
              <span class="d-block">{{ date('d-m-Y H:i:s', strtotime($info->resp->dateTime)) }}</span>
            </div>
          </div>
        @endif
      </div>
    @endif
  </main>

  <script type="text/php">
    if ( isset($pdf) ) {
      $pdf->page_script('
        $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
        $pdf->text(510, 35, "PÁGINA $PAGE_NUM DE $PAGE_COUNT", $font, 10);
        $pdf->image("images/logo-banco-inv.png", 30, 20, 87, 36);
        $pdf->text(445, 800, "FECHA: ".date("d-m-Y H:i:s"), $font, 10);
        $pdf->text(20, 800, "USUARIO: {{$info->username}}", $font, 10);
        $pdf->text(20, 820, "10 CALLE A 0-79 ZONA 10 CIUDAD DE GUATEMALA",  $font, 8);
        $pdf->text(330, 820, "INV@INV.COM.GT",  $font, 8);
        $pdf->text(517, 820, "(502) 2315-6600",  $font, 8);
      ');
    }
  </script>
</body>
</html>