<!DOCTYPE html>
<html lang="es">
<head>
  <title>Banco INV</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <style>
    @page {
      margin: 5cm 0cm 1.5cm;
      font-family: Arial, sans-serif;
    }
    body {
      font-family: Arial, sans-serif;
      margin: 1cm 0.5cm 1cm;
      color: #555;
      font-size: 13px;
    }
    header {
      position: fixed;
      top: 0;
      left: 0cm;
      right: 0cm;
      background-color: #fff;
      text-align: left;
      margin-top: -3cm
    }
    footer {
      position: fixed;
      bottom: 0cm;
      left: 0cm;
      right: 0cm;
      height: 2cm;
      background-color: #2a0927;
      text-align: center;
      line-height: 35px;
    }
    #resumen{
      letter-spacing: 1px;
      border: 1px solid #eee;
      padding: 0.25cm 0.5cm;
    }
    .logo{
      width: 4.5cm
    }
    .title{
      text-align: center;
      background: #f1f1f1;
      text-transform: uppercase;
      padding: 0.15cm 1cm;
      letter-spacing: 1px;
    }
    #info{
      padding-bottom: 0;
      text-transform: uppercase;
      line-height: 0.75;
      width: 100%;
      position: relative;
    }
    #info:first-child{
      margin-top: 1cm;
    }
    #info label{
      font-weight: bold;
    }
    #info>div{
      width: 50%;
      display: inline-block;
      font-size: 11px;
      letter-spacing: 1px;
    }
    .text-right{
      text-align: right;
    }
    .text-left{
      text-align: left;
    }
    table {
      border-collapse: collapse;
      width: 100%;
      font-size: 11px;
      text-transform: uppercase;
    }
    thead{
      background: #518844;
      color: #fff;
      text-transform: uppercase;
      font-size: 12px;
    }
    td, th {
      text-align: left;
      padding: 8px;
    }
    tr:nth-child(even) {
      background-color: #eee;
    }
  </style>
</head>
<body>
  <header>
    <h3 class="title">Historial de Pagos a Empleados</h3>
    <div id="info">
      <div class="col-6 text-right">
        <b>Nombre de planilla:</b>
      </div>
      <div class="col-6 text-left">
        {{ $info->payroll->name }}
      </div>
    </div>
    <div id="info">
      <div class="col-6 text-right">
        <b>Cuenta:</b>
      </div>
      <div class="col-6 text-left">
        {{ $currency[$info->payroll->currency][0] }} {{ $info->payroll->account }}
      </div>
    </div>
    <div id="info">
      <div class="col-6 text-right">
        <b>Fecha:</b>
      </div>
      <div class="col-6 text-left">
        {{ join('-',array_reverse(explode('-',$info->accounts->rows[0]->date))) }}
      </div>
    </div>
  </header>
  <main>
  <table style="max-width: 12cm!important">
      <thead>
        <tr>
          <th style="width: 0.5cm">Id</th>
          <th style="width: 1.25cm">Código</th>
          <th style="width: 4.755cm">Nombre</th>
          <th style="width: 2cm">Puesto</th>
          <th>Cuenta</th>
          <th>Banco</th>
          <th class="text-right">Salario</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($info->accounts->rows as $t)
        <tr>
          <td style="width: 0.5cm">{{ $t->id }}</td>
          <td style="width: 1.25cm">{{ $t->employeeId }}</td>
          <td style="width: 4.75cm">{{ $t->name }}</td>
          <td style="width: 2cm">{{ $t->position }}</td>
          <td> {{ ($t->accountType == 1 ? 'MON-' : 'AHO-') . ($t->currency == 'QUE'?'GTQ':'USD') . '-'.$t->account }}</td>
          <td>{{ $t->bank }}</td>
          <td class="text-right">{{ $currency[$t->currency][0]}}{{ number_format($t->amount,2)  }}</td>
        </tr>
        <?php $total += $t->amount; ?>
      @endforeach
      </tbody>
      <tfoot>
        <td></td><td></td><td></td><td></td>
        <td class="text-right" colspan="3">Pago Total Planilla: <b>{{ $currency[$t->currency][0]}}{{ number_format($total,2)  }}</b></td>
      </tfoot>
    </table>
  </main>

  <script type="text/php">
    if ( isset($pdf) ) {
      $pdf->page_script('
        $font = $fontMetrics->get_font("Arial, Helvetica, sans-serif", "normal");
        $pdf->text(510, 35, "PÁGINA $PAGE_NUM DE $PAGE_COUNT", $font, 10);
        $pdf->image("images/logo-banco-inv.png", 30, 20, 87, 36);
        $pdf->text(445, 800, "FECHA: ".date("d-m-Y H:i:s"), $font, 10);
        $pdf->text(20, 800, "USUARIO: {{$info->accounts->username ?? ''}}", $font, 10);
        $pdf->text(20, 820, "10 CALLE A 0-79 ZONA 10 CIUDAD DE GUATEMALA",  $font, 8);
        $pdf->text(330, 820, "INV@INV.COM.GT",  $font, 8);
        $pdf->text(517, 820, "(502) 2315-6600",  $font, 8);
      ');
    }
  </script>
</body>
</html>