<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\ApiController;

class PDFController extends Controller {

  public function estadoDeCuenta(Request $request){
    $currency = [
      'QUE' => ['Q.', 'Quetzales', 'GTQ'],
      'US$' => ['$.', 'Dólares', 'USD']
    ];
    $data = [
      'title' => 'Estado de Cuenta',
      'account' => json_decode($request->account),
      'balance' => $request->balance,
      'start' => date('d-m-Y', strtotime($request->start)),
      'end' => date('d-m-Y', strtotime($request->end)),
      'transactions' => json_decode($request->table),
      'currency' => $currency,
      'username' => $request->username,
      'mov' => [
        'debito' => [0, 0],
        'credito' => [0,0],
        'cheques'=> [0, 0]
      ]
    ];
    $pdf =  PDF::loadView('pdf-edo-cuenta', $data);
    return $pdf->stream('BANCOINV_'.$currency[$data['account']->currency][2].'_'.strtoupper(substr($data['account']->typeDesc,0,3)).'_'. $data['account']->number.'.pdf');
  }

  public function transaccionRealizada(Request $request){
    $currency = [
      'QUE' => ['Q.', 'Quetzales', 'GTQ'],
      'US$' => ['$.', 'Dólares', 'USD']
    ];
    $data = [
      'info' => json_decode($request->info),
      'currency' => $currency
    ];
    $pdf =  PDF::loadView('pdf-transaccion-realizada', $data);
    return $pdf->stream('BANCOINV_TRANSACCION.pdf');
  }

  public function historialPagosEmpleados(Request $request){
    $currency = [
      'QUE' => ['Q.', 'Quetzales', 'GTQ'],
      'US$' => ['$.', 'Dólares', 'USD']
    ];
    $data = [
      'info' => json_decode($request->info),
      'currency' => $currency,
      'total' => 0
    ];
    //$total = $data['info']->
    $pdf =  PDF::loadView('pdf-historial-pagos-empleados', $data);
    return $pdf->stream('BANCOINV_HISTORIAL_PAGO_EMPLEADOS.pdf');
  }

  public function pagoProveedoresRealizada(Request $request){
    $currency = [
      'QUE' => ['Q.', 'Quetzales', 'GTQ'],
      'US$' => ['$.', 'Dólares', 'USD']
    ];
    $data = [
      'info' => json_decode($request->info),
      'currency' => $currency
    ];
    $pdf =  PDF::loadView('pdf-pago-proveedores-realizada', $data);
    return $pdf->stream('BANCOINV_PAGO_PROVEEDORES.pdf');
  }
}
