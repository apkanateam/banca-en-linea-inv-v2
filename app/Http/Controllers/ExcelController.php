<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\ApiController;

class PDFController extends Controller {

  public function index(Request $request){

    
    header('Content-type: application/vnd.ms-excel;charset=iso-8859-15');
    header('Content-Disposition: attachment; filename=nombre_archivo.xls');

    $currency = [
      'QUE' => ['Q.', 'Quetzales'],
      'US$' => ['$.', 'Dólares']
    ];
    $data = [
      'title' => 'Estado de Cuenta',
      'account' => json_decode($request->account),
      'balance' => $request->balance,
      'start' => $request->start,
      'end' => $request->end,
      'transactions' => json_decode($request->table),
      'currency' => $currency,
      'username' => $request->username,
      'mov' => [
        'debito' => [0, 0],
        'credito' => [0,0],
        'cheques'=> [0, 0]
      ]
    ];
    
    return view('pdf', $data);
    //$pdf =  PDF::loadView('pdf', $data);
    //return $pdf->stream('estado-de-cuenta.pdf');
  }
}
