<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Concerns\InteractsWithInput;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Cookie;

class ApiController extends Controller {
  private $api_url = 'http://192.168.20.69/InvAPI/';
  private $client_id = 'INVFrnt';

  public function __construct(){
    $this->client = new \GuzzleHttp\Client(['base_uri' => $this->api_url]);
  }

  /* AUTH API */
  public function signIn(Request $request) { 
    try {
      $res = $this->client->post('token', [
        'form_params' => [
          'client_id' => $this->client_id,
          'grant_type' => 'password',
          'username' => $request->username,
          'password' => $request->password,
          'profile' => $request->profile
        ]
      ]);
      $result = json_decode($res->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }
    return $result;
  }

  public function refreshToken(Request $request){
    try {
      $res = $this->client->post('token', [
        'form_params' => [
          'client_id' => $this->client_id,
          'grant_type' => 'refresh_token',
          'refresh_token' => $request->refresh_token
        ]
      ]);
      $result = json_decode($res->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }
    return $result;
  }

  /* --------- PROFILE --------- */
  public function getProfile(Request $request){
    try{
      $response = $this->client->get('api/Profile/', [
        'query' => [
          'username' => $request->username
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getProfileStatus(Request $request){
    try{
      $response = $this->client->get('api/Profile/ProfileStatus', [
        'query' => [
          'username' => $request->username
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getProfileImageList(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Profile/ImageList', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postProfileImage(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->post('api/Profile/SaveProfileImage', [
        'headers' => $headers,
        'json' => [
          'phrase' => $request->phrase,
          'image' => $request->image
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postProfileAnswers(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->post('api/Profile/SaveAnswers', [
        'headers' => $headers,
        'json' => $request->answers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postProfileUpdatePassword(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->post('api/Profile/UpdatePassword', [
        'headers' => $headers,
        'json' => [
          'current' => $request->current,
          'new' => $request->new
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postProfileSendPasswordReset(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->post('api/Profile/SendPasswordReset', [
        'headers' => $headers,
        'json' => [
          'username' => $request->username,
          'method' => $request->method
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getProfileRecoveryQuestions(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Profile/RecoveryQuestions', [
        'headers' => $headers,
        'query' => [
          'username' => $request->username
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getProfileRecoveryQuestionsList(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Profile/RecoveryQuestionsList', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getProfileRecoveryMethods(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Profile/RecoveryMethods', [
        'headers' => $headers,
        'query' => [
          'username' => $request->username
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postProfilePasswordRecovery(Request $request){
    $headers = [
      'Accept' => 'application/json'
    ];
    try{
      $json = [
        'username' => $request->username,
        'method' => $request->method
      ];
      if ($request->answers){
        $json['answers'] = $request->answers;
      }
      $response = $this->client->post('api/Profile/PasswordRecovery', [
        'headers' => $headers,
        'json' => $json
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getProfilePersonal(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Profile/Personal', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postProfilePersonalUpdate(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $json = [
        'email' => $request->email,
        'phone' => $request->phone,
        'address' => $request->address,
        'income' => $request->income,
        'outcome' => $request->outcome,
        'workPhone1' => $request->workPhone1,
        'workAddress' => $request->workAddress
      ];
      $response = $this->client->post('api/Profile/Personal/UpdateInfo', [
        'headers' => $headers,
        'json' => array_filter($json)
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  /* --------------------------- */

  /* --------- ACCOUNTS -------- */
  public function getAccountsSavings(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Accounts/Savings/', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsSavingsBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Savings/Balance', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsSavingsMonthAverage(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Savings/MonthAverage', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account,
          'months' => $request->months
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsSavingsTransactions(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Savings/Transactions', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account,
          'startDate' => $request->startDate,
          'endDate' => $request->endDate
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsMonetary(Request $request){
    $headers = [
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    ];
    try{
      $response = $this->client->get('api/Accounts/Monetary/', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsMonetaryBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Monetary/Balance', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsMonetaryMonthAverage(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Monetary/MonthAverage', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account,
          'months' => $request->months
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsMonetaryTransactions(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Monetary/Transactions', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account,
          'startDate' => $request->startDate,
          'endDate' => $request->endDate
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsMonetaryCheckImage(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Monetary/CheckImage', [
        'headers' => $headers,
        'query' => [
          'account' => $request->account,
          'document' => $request->document,
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postAccountsMonetaryBlockCheck(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Accounts/Monetary/BlockCheck', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'start' => $request->start,
          'end' => $request->finish
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsLoans(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Loans', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsLoansInfo(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Loans/Info', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsLoansQuote(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Loans/Quote', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsLoansAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Loans/Accounts', [
        'headers' => $headers,
        'query' => [
          'currency' => $request->currency
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsLoansTransactions(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Loans/Transactions', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postAccountsLoansConfigureAutoPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Accounts/Loans/ConfigureAutoPayment', [
        'headers' => $headers,
        'json' => [
          'loanId' => $request->loanId,
          'amount' => $request->amount,
          'day' => $request->day,
          'account' => $request->account,
          'enablePayment' => $request->enablePayment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postAccountsRequestsCheckbook(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Accounts/Requests/Checkbook', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'type' => $request->type,
          'quantity' => $request->quantity
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postAccountsRequestsDebitCard(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Accounts/Requests/DebitCard', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'type' => $request->type,
          'amount' => $request->amount,
          'addressType' => $request->addressType,
          'address' => $request->address
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postAccountsRequestsCashierCheck(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Accounts/Requests/CashierCheck', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'agency' => $request->agency,
          'amount' => $request->amount,
          'currency' => $request->currency,
          'recipient' => $request->recipient
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getAccountsRequestsAccountsForLoan(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Accounts/Requests/AccountsForLoan', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postAccountsRequestsLoan(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Accounts/Requests/Loan', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'amount' => $request->amount,
          'currency' => $request->currency,
          'months' => $request->months,
          'phone' => $request->phone,
          'email' => $request->email,
          'reason' => $request->reason,
          'guarantor' => $request->guarantor,
          'guarantorPhone' => $request->guarantorPhone
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  /* --------------------------- */

  /* ----- TRANSFERENCIAS ------ */
  public function getTransfersOwnAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Own/Accounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersOwnTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Own/Transfer', [
        'headers' => $headers,
        'json' => [
          'origin' => $request->origin,
          'destination' => $request->destination,
          'amount' => $request->amount,
          'description' => $request->description
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseOwnAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Own/Accounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseOwnTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Own/Transfer', [
        'headers' => $headers,
        'json' => [
          'email' => $request->email,
          'origin' => $request->origin,
          'destination' => $request->destination,
          'amount' => $request->amount,
          'description' => $request->description
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseTransactions(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Transactions', [
        'headers' => $headers,
        'query' => [
          'type' => $request->type
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseTransactionDetail(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/TransactionDetail', [
        'headers' => $headers,
        'query' => [
          'sequence' => $request->sequence
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseExecute(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Execute', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseReject(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Reject', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseVerify(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Verify', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersOthersAssociatedAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Others/AssociatedAccounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersOthersTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Others/Transfer', [
        'headers' => $headers,
        'json' => [
          'email' => $request->email,
          'origin' => $request->origin,
          'destination' => $request->destination,
          'amount' => $request->amount,
          'description' => $request->description
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersOthersValidateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Others/ValidateAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersOthersAddAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Others/AddAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'alias' => $request->alias
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersOthersUpdateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Others/UpdateAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'alias' => $request->alias
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersOthersDeleteAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Others/DeleteAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseOthersAssociatedAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Others/AssociatedAccounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseOthersTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Others/Transfer', [
        'headers' => $headers,
        'json' => [
          'email' => $request->email,
          'origin' => $request->origin,
          'destination' => $request->destination,
          'amount' => $request->amount,
          'description' => $request->description
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseOthersAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Others/Accounts', [
        'headers' => $headers,
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseOthersValidateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Others/ValidateAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseOthersAddAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Others/AddAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'alias' => $request->alias
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseOthersUpdateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Others/UpdateAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'alias' => $request->alias
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseOthersDeleteAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Others/DeleteAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersBanksAssociatedAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Banks/AssociatedAccounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersBanksAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Banks/Accounts', [
        'headers' => $headers,
        'query' => [
          'currency' => $request->currency
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersBanksValidateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Banks/ValidateAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'bank' => $request->bank,
          'accounttype' => $request->accountType,
          'currency' => $request->currency
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersBanksAddAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Banks/AddAccount', [
        'headers' => $headers,
        'json' => [
          'bank' => $request->bank,
          'accountType' => $request->accountType,
          'account' => $request->account,
          'currency' => $request->currency,
          'alias' => $request->alias,
          'idType' => $request->idType,
          'idNumber' => $request->idNumber,
          'email' => $request->email,
          'phone' => $request->phone
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersBanksTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Banks/Transfer', [
        'headers' => $headers,
        'json' => [
          "origin" => $request->origin,
          "destination" => $request->destination,
          "amount" => $request->amount,
          "comment" => $request->comment,
          "bank" => $request->bank,
          "currency" => $request->currency
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersBanksUpdateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Banks/UpdateAccount', [
        'headers' => $headers,
        'json' => [
          'bank' => $request->bank,
          'accountType' => $request->accountType,
          'account' => $request->account,
          'currency' => $request->currency,
          'alias' => $request->alias,
          'idType' => $request->idType,
          'idNumber' => $request->idNumber,
          'email' => $request->email,
          'phone' => $request->phone
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersBanksDeleteAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Banks/DeleteAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'bank' => $request->bank
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseBanksAssociatedAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Banks/AssociatedAccounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseBanksAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Banks/Accounts', [
        'headers' => $headers,
        'query' => [
          'currency' => $request->currency
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseBanksValidateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Banks/ValidateAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'bank' => $request->bank,
          'accounttype' => $request->accountType,
          'currency' => $request->currency
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseBanksAddAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Banks/AddAccount', [
        'headers' => $headers,
        'json' => [
          'bank' => $request->bank,
          'accountType' => $request->accountType,
          'account' => $request->account,
          'currency' => $request->currency,
          'alias' => $request->alias,
          'idType' => $request->idType,
          'email' => $request->email,
          'phone' => $request->phone,
          'idNumber' => $request->idNumber
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    } 
    return $result;
  }
  public function postTransfersEnterpriseBanksTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Banks/Transfer', [
        'headers' => $headers,
        'json' => [
          "origin" => $request->origin,
          "destination" => $request->destination,
          "amount" => $request->amount,
          "comment" => $request->comment,
          "bank" => $request->bank,
          "currency" => $request->currency
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseBanksUpdateAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Banks/UpdateAccount', [
        'headers' => $headers,
        'json' => [
          'bank' => $request->bank,
          'accountType' => $request->accountType,
          'account' => $request->account,
          'currency' => $request->currency,
          'alias' => $request->alias,
          'idType' => $request->idType,
          'email' => $request->email,
          'phone' => $request->phone,
          'idNumber' => $request->idNumber
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    } 
    return $result;
  }
  public function postTransfersEnterpriseBanksDeleteAccount(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Banks/DeleteAccount', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'bank' => $request->bank
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseLoansTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Loans/Transfer', [
        'headers' => $headers,
        'json' => [
          'origin' => $request->origin,
          'destination' => $request->destination,
          'quotes' => $request->quotes
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersExchangeHistory(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Exchange/History', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersExchangeRate(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Exchange/Rate', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersExchangeTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Exchange/Transfer', [
        'headers' => $headers,
        'json' => [
          'origin' => $request->origin,
          'destination' => $request->destination,
          'amount' => $request->amount,
          'description' => $request->description
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function postTransfersLoansTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Loans/Transfer', [
        'headers' => $headers,
        'json' => [
          'origin' => $request->origin,
          'destination' => $request->destination,
          'quotes' => $request->quotes
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function getTransfersServices(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Services', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function getTransfersServicesAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Services/Accounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function getTransfersServicesAvailable(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Services/Available', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function postTransfersServicesValidateService(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Services/ValidateService', [
        'headers' => $headers,
        'json' => [
          'categoryCode'=> $request->categoryCode,
          'serviceCode'=> $request->serviceCode,
          'account'=> $request->account
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersServicesAddService(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Services/AddService', [
        'headers' => $headers,
        'json' => [
          'alias'=> $request->alias,
          'categoryCode'=> $request->categoryCode,
          'serviceCode'=> $request->serviceCode,
          'account'=> $request->account
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersServicesDeleteService(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Services/DeleteService', [
        'headers' => $headers,
        'json' => [
          'id'=> $request->id
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrolls(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsDetail(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/Detail', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsHistory(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/History', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id,
          'start' => $request->start,
          'end' => $request->end
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsPending(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/Pending', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsBanks(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/Banks', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsAdd(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Add', [
        'headers' => $headers,
        'json' => [
          'name' => $request->name,
          'account' => $request->account,
          'auto' => $request->auto,
          'payDay' => $request->payDay
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsUpdate(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Update', [
        'headers' => $headers,
        'json' => [
          'id' => $request->id,
          'name' => $request->name,
          'account' => $request->account,
          'auto' => $request->auto,
          'payDay' => $request->payDay
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsDelete(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Delete', [
        'headers' => $headers,
        'json' => [
          'id' => $request->id,
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsAddPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/AddPayment', [
        'headers' => $headers,
        'json' => [
          'payrollId' => $request->payrollId
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsEmployees(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/Employees', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsEmployeeDetails(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/Employee/Details', [
        'headers' => $headers,
        'query' => [
          'payrollId' => $request->payrollId,
          'employeeId' => $request->employeeId
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsEmployeePayList(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/EmployeePaylist', [
        'headers' => $headers,
        'query' => [
          'payrollId' => $request->payrollId,
          'payId' => $request->payId
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsEmployeePayments(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/Employee/Payments', [
        'headers' => $headers,
        'query' => [
          'payrollId' => $request->payrollId,
          'employeeId' => $request->employeeId,
          'months' => $request->months
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersPayrollsEmployeePayDetail(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Payrolls/EmployeePayDetail', [
        'headers' => $headers,
        'query' => [
          'payrollId' => $request->payrollId,
          'payId' => $request->payId,
          'employeePayId' => $request->employeePayId
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsEmployeeAdd(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Employee/Add', [
        'headers' => $headers,
        'json' => [
          'payrollId' => $request->payrollId,
          'name' => $request->name,
          'position' => $request->position,
          'bankCode' => $request->bankCode,
          'account' => $request->account,
          'accountType' => $request->accountType,
          'currency' => $request->currency,
          'amount' => $request->amount
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsEmployeeUpdate(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Employee/Update', [
        'headers' => $headers,
        'json' => [
          'payrollId' => $request->payrollId,
          'employeeId' => $request->employeeId,
          'name' => $request->name,
          'position' => $request->position,
          'bankCode' => $request->bankCode,
          'account' => $request->account,
          'accountType' => $request->accountType,
          'currency' => $request->currency,
          'amount' => $request->amount
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsEmployeeAddMultiple(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Employee/AddMultiple', [
        'headers' => $headers,
        'json' => [
          'payrollId' => $request->payrollId,
          'employees' => $request->employees
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsEmployeeDelete(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Employee/Delete', [
        'headers' => $headers,
        'json' => [
          'payrollId' => $request->payrollId,
          'employeeId' => $request->employeeId
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsExecute(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Execute', [
        'headers' => $headers,
        'json' => [
          'paymentId' => $request->paymentId,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsVerify(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Verify', [
        'headers' => $headers,
        'json' => [
          'paymentId' => $request->paymentId,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersPayrollsReject(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Payrolls/Reject', [
        'headers' => $headers,
        'json' => [
          'paymentId' => $request->paymentId,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersProviders(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Providers', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersProvidersAdd(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Providers/Add', [
        'headers' => $headers,
        'json' => [
          'name' => $request->name,
          'service' => $request->service, 
          'account' => $request->account,
          'accountType' => $request->accountType,
          'bank' => $request->bank,
          'currency' => $request->currency
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersProvidersAddPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Providers/AddPayment', [
        'headers' => $headers,
        'json' => [
          'providerId' => $request->providerId,
          'account' => $request->account,
          'amount' => $request->amount,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersProvidersDelete(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Providers/Delete', [
        'headers' => $headers,
        'json' => [
          'id' => $request->id,
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersProvidersAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Providers/Accounts', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersProvidersHistory(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Providers/History', [
        'headers' => $headers,
        'query' => [
          'id' => $request->id,
          'start' => $request->start,
          'end' => $request->end
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersProvidersHistoryAll(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Providers/HistoryAll', [
        'headers' => $headers,
        'query' => [
          'start' => $request->start,
          'end' => $request->end
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }






  public function postTransfersProvidersExecute(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Providers/Execute', [
        'headers' => $headers,
        'json' => [
          'paymentId' => $request->paymentId,
          'comment' => $request->comment,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersProvidersVerify(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Providers/Verify', [
        'headers' => $headers,
        'json' => [
          'paymentId' => $request->paymentId,
          'comment' => $request->comment,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersProvidersReject(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Providers/Reject', [
        'headers' => $headers,
        'json' => [
          'paymentId' => $request->paymentId,
          'comment' => $request->comment,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function getTransfersEnterpriseExchangeHistory(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Exchange/History', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseExchangeAccounts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Exchange/Accounts', [
        'headers' => $headers,
        'query' => [
          'mode' => $request->mode
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postTransfersEnterpriseExchangeTransfer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Transfers/Enterprise/Exchange/Transfer', [
        'headers' => $headers,
        'json' => [
          'email' => $request->email,
          'origin' => $request->origin,
          'destination' => $request->destination,
          'amount' => $request->amount,
          'description' => $request->description
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function postServicesClaroBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Claro/Balance', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesClaroPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Claro/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'amount' => $request->amount
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesClaroSuperPacksProducts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/ClaroSuperPacks/Products', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesClaroSuperPacksPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/ClaroSuperPacks/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'productCode' => $request->productCode,
          'amount' => $request->amount,
          'name' => $request->name,
          'nit' => $request->nit,
          'email' => $request->email
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesClaroRecargaProducts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/RecargaClaro/Products', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesClaroRecargaPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/RecargaClaro/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'productCode' => $request->productCode,
          'amount' => $request->amount,
          'name' => $request->name,
          'nit' => $request->nit,
          'email' => $request->email
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function postServicesTigoBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Tigo/Balance', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Tigo/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'amount' => $request->amount
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoRecargaProducts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/RecargaTigo/Products', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoRecargaPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/RecargaTigo/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'productCode' => $request->productCode,
          'amount' => $request->amount,
          'name' => $request->name,
          'nit' => $request->nit,
          'email' => $request->email
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoPaquetigosProducts(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Paquetigos/Products', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoPaquetigosPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Paquetigos/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'productCode' => $request->productCode,
          'amount' => $request->amount,
          'name' => $request->name,
          'nit' => $request->nit,
          'email' => $request->email
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoTigoStarBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/TigoStar/Balance', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesTigoTigoStarPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/TigoStar/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          'account' => $request->account,
          'amount' => $request->amount
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEegsaBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Eegsa/Balance', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEegsaPayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Eegsa/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          "document" => $request->document,
          'account' => $request->account,
          'amount' => $request->amount
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEegsaPaymentReconnect(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Eegsa/PaymentReconect', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          "document" => $request->document,
          'account' => $request->account,
          'amount' => $request->amount
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEegsaBill(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Eegsa/Bill', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          "type" => $request->type,
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEegsaBillReconnect(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Eegsa/BillReconect', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEnerguateBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Energuate/Balance', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postServicesEnerguatePayment(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Services/Energuate/Payment', [
        'headers' => $headers,
        'json' => [
          'clientId' => $request->clientId,
          "type" => $request->type,
          'company' => $request->company,
          'account' => $request->account,
          'amount' => $request->amount
        ],
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function getManagementLockDisclaimer(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/LockDisclaimer', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementLockAccountBalance(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/LockAccountBalance', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'amount' => $request->amount,
          'comment' => $request->comment 
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCheckbooksRequest(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Checkbooks/Request', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'type' => $request->type,
          'register' => $request->register,
          'quantity' => $request->quantity
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getManagementEnterpriseCheckbooksList(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/Enterprise/Checkbooks/List', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getManagementEnterpriseCheckbooksDetail(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/Enterprise/Checkbooks/Detail', [
        'headers' => $headers,
        'query' => [
          'sequence' => $request->sequence
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCheckbooksExecute(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Checkbooks/Execute', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCheckbooksVerify(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Checkbooks/Verify', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCheckbooksReject(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Checkbooks/Reject', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }



  public function postManagementEnterpriseCashierchecksRequest(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Cashierchecks/Request', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'agency' => $request->agency,
          'amount' => $request->amount,
          'currency' => $request->currency,
          'recipient' => $request->recipient
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getManagementEnterpriseCashierchecksList(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/Enterprise/Cashierchecks/List', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getManagementEnterpriseCashierchecksDetail(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/Enterprise/Cashierchecks/Detail', [
        'headers' => $headers,
        'query' => [
          'sequence' => $request->sequence
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCashierchecksExecute(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Cashierchecks/Execute', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCashiercheckssVerify(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Cashierchecks/Verify', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseCashierchecksReject(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Cashierchecks/Reject', [
        'headers' => $headers,
        'json' => [
          'sequence' => $request->sequence,
          'comment' => $request->comment
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getManagementEnterpriseFactoringLines(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/Enterprise/Factoring/Lines', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getManagementEnterpriseFactoringList(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Management/Enterprise/Factoring/List', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postManagementEnterpriseFactoringRequest(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/Management/Enterprise/Factoring/Request', [
        'headers' => $headers,
        'json' => [
          'account' => $request->account,
          'description' => $request->description,
          'provider' => $request->provider,
          'reference' => $request->reference,
          'amount' => $request->amount,
          'endDate' => $request->endDate,
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersBanksAvailable(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Banks/Available', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getTransfersEnterpriseBanksAvailable(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/Transfers/Enterprise/Banks/Available', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  /* --------------------------- */

  /* --------- GENERAL --------- */
  public function getGeneralFavoritesLoans(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/General/Favorites/Loans', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postGeneralFavoritesLoan(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/General/Favorites/Loan', [
        'headers' => $headers,
        'json' => [
          'number' => $request->number,
          'status' => $request->status
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }

  public function getGeneralFavoritesServices(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/General/Favorites/Services', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postGeneralFavoritesService(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/General/Favorites/Service', [
        'headers' => $headers,
        'json' => [
          'type' => $request->type,
          'status' => $request->status,
          'account' => $request->account,
          'service' => $request->service
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getGeneralFavoritesPayrolls(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/General/Favorites/Payrolls', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postGeneralFavoritesPayroll(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/General/Favorites/Payroll', [
        'headers' => $headers,
        'json' => [
          'id' => $request->id,
          'status' => $request->status
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function getGeneralFavoritesProviders(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->get('api/General/Favorites/Providers', [
        'headers' => $headers
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
  public function postGeneralFavoritesProvider(Request $request){
    $headers = array(
      'Authorization' => 'Bearer ' . $request->header('Authorization-Bearer'),
      'Accept' => 'application/json'
    );
    try{
      $response = $this->client->post('api/General/Favorites/Provider', [
        'headers' => $headers,
        'json' => [
          'id' => $request->id,
          'status' => $request->status
        ]
      ]);
      $result = json_decode($response->getBody()->getContents(), true);  
    } catch (BadResponseException $e) {
      $result = json_decode($e->getResponse()->getBody()->getContents(), true);
    }  
    return $result;
  }
}


