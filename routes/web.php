<?php

use Illuminate\Support\Facades\Route;

/* GLOBAL */
Route::group(['prefix' => 'pdf'], function(){
  Route::post('/estado-de-cuenta', 'PDFController@estadoDeCuenta');
  Route::post('/transaccion-realizada', 'PDFController@transaccionRealizada');
  Route::post('/historial-pagos-empleados', 'PDFController@historialPagosEmpleados');
  Route::post('/pago-proveedores-realizada', 'PDFController@pagoProveedoresRealizada');
});

Route::get('/{any}', 'SpaController@index')->where('any', '.*');
