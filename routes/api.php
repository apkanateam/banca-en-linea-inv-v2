<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/* ----- AUTH LOGIN ----- */
Route::group(['prefix' => 'auth'], function(){
	// INICIO DE SESIÓN Y RECUPERACIÓN DE CONTRASEÑAS
	Route::post('/signin', 'ApiController@signIn');
	Route::post('/refresh-token', 'ApiController@refreshToken');
});
/* ---------------------- */

/* ------- PERFIL ------- */
Route::group(['prefix' => 'profile'], function(){
	Route::get('', 'ApiController@getProfile');
	Route::get('/status', 'ApiController@getProfileStatus');
	Route::get('/image-list', 'ApiController@getProfileImageList');
	Route::post('/save-profile-image', 'ApiController@postProfileImage');
	Route::post('/save-answers', 'ApiController@postProfileAnswers');
	Route::post('/update-password', 'ApiController@postProfileUpdatePassword');
	Route::post('/send-password-reset', 'ApiController@postProfileSendPasswordReset');
	Route::get('/recovery-questions', 'ApiController@getProfileRecoveryQuestions');
	Route::get('/recovery-questions-list', 'ApiController@getProfileRecoveryQuestionsList');
	Route::get('/recovery-methods', 'ApiController@getProfileRecoveryMethods');
	Route::post('/password-recovery', 'ApiController@postProfilePasswordRecovery');
	Route::get('/personal', 'ApiController@getProfilePersonal');
	Route::post('/personal/update', 'ApiController@postProfilePersonalUpdate');

});
/* ---------------------- */


/* ------- CUENTAS ------ */
Route::group(['prefix' => 'accounts'], function(){
	// CUENTAS DE AHORRO
	Route::get('/savings', 'ApiController@getAccountsSavings');
	Route::get('/savings/balance', 'ApiController@getAccountsSavingsBalance');
	Route::get('/savings/month-average', 'ApiController@getAccountsSavingsMonthAverage');
	Route::get('/savings/transactions', 'ApiController@getAccountsSavingsTransactions');

	// CUENTAS MONETARIAS
	Route::get('/monetary', 'ApiController@getAccountsMonetary');
	Route::get('/monetary/balance', 'ApiController@getAccountsMonetaryBalance');
	Route::get('/monetary/month-average', 'ApiController@getAccountsMonetaryMonthAverage');
	Route::get('/monetary/transactions', 'ApiController@getAccountsMonetaryTransactions');
	Route::get('/monetary/check-image', 'ApiController@getAccountsMonetaryCheckImage');
	Route::post('/monetary/block-check', 'ApiController@postAccountsMonetaryBlockCheck');

	// PRESTAMOS
	Route::get('/loans', 'ApiController@getAccountsLoans');
	Route::get('/loans/info', 'ApiController@getAccountsLoansInfo');
	Route::get('/loans/quote', 'ApiController@getAccountsLoansQuote');
	Route::get('/loans/accounts', 'ApiController@getAccountsLoansAccounts');
	Route::get('/loans/transactions', 'ApiController@getAccountsLoansTransactions');
	Route::post('/loans/configure-auto-payment', 'ApiController@postAccountsLoansConfigureAutoPayment') ;
	// REQUESTS
	Route::post('/requests/checkbook', 'ApiController@postAccountsRequestsCheckbook');
	Route::post('/requests/debit-card', 'ApiController@postAccountsRequestsDebitCard');
	Route::post('/requests/cashier-check', 'ApiController@postAccountsRequestsCashierCheck');
	Route::get('/requests/accounts/accounts-for-loan', 'ApiController@getAccountsRequestsAccountsForLoan');
	Route::post('/requests/loan', 'ApiController@postAccountsRequestsLoan');
});
/* ---------------------- */


/* --- TRANSFERENCIAS --- */
Route::group(['prefix' => 'transfers'], function(){
	// CUENTAS PROPIAS - PERSONA
	Route::get('/own/accounts', 'ApiController@getTransfersOwnAccounts');
	Route::post('/own/transfer', 'ApiController@postTransfersOwnTransfer');
	
	// CUENTAS DE TERCEROS- PERSONA
	Route::get('/others/associated-accounts', 'ApiController@getTransfersOthersAssociatedAccounts');
	Route::post('/others/transfer', 'ApiController@postTransfersOthersTransfer');
	Route::post('/others/validate-account', 'ApiController@postTransfersOthersValidateAccount');
	Route::post('/others/add-account', 'ApiController@postTransfersOthersAddAccount');
	Route::post('/others/update-account', 'ApiController@postTransfersOthersUpdateAccount');
	Route::post('/others/delete-account', 'ApiController@postTransfersOthersDeleteAccount');

	// OTROS BANCOS - PERSONA
	Route::get('/banks/available', 'ApiController@getTransfersBanksAvailable');
	Route::get('/banks/accounts', 'ApiController@getTransfersBanksAccounts');
	Route::get('/banks/associated-accounts', 'ApiController@getTransfersBanksAssociatedAccounts');
	Route::post('/banks/validate-account', 'ApiController@postTransfersBanksValidateAccount');
	Route::post('/banks/add-account', 'ApiController@postTransfersBanksAddAccount');
	Route::post('/banks/transfer', 'ApiController@postTransfersBanksTransfer');
	Route::post('/banks/update-account', 'ApiController@postTransfersBanksUpdateAccount');
	Route::post('/banks/delete-account', 'ApiController@postTransfersBanksDeleteAccount');
	
	// DIVISAS - PERSONA
	Route::get('/exchange/history', 'ApiController@getTransfersExchangeHistory');
	Route::get('/exchange/rate', 'ApiController@getTransfersExchangeRate');
	Route::post('/exchange/transfer', 'ApiController@postTransfersExchangeTransfer');

	// PRESTAMOS
	Route::post('/loans/transfer', 'ApiController@postTransfersLoansTransfer');

	// SERVICIOS
	Route::get('/services', 'ApiController@getTransfersServices');
	Route::get('/services/accounts', 'ApiController@getTransfersServicesAccounts');
	Route::get('/services/available', 'ApiController@getTransfersServicesAvailable');
	Route::post('/services/validate-service', 'ApiController@postTransfersServicesValidateService');
	Route::post('/services/add-service', 'ApiController@postTransfersServicesAddService');
	Route::post('/services/delete-service', 'ApiController@postTransfersServicesDeleteService');

	// PLANILLAS
	Route::get('/payrolls', 'ApiController@getTransfersPayrolls');
	Route::get('/payrolls/detail', 'ApiController@getTransfersPayrollsDetail');
	Route::get('/payrolls/history', 'ApiController@getTransfersPayrollsHistory');
	Route::get('/payrolls/pending', 'ApiController@getTransfersPayrollsPending');
	Route::get('/payrolls/banks', 'ApiController@getTransfersPayrollsBanks');
	Route::post('/payrolls/add', 'ApiController@postTransfersPayrollsAdd');
	Route::post('/payrolls/update', 'ApiController@postTransfersPayrollsUpdate');
	Route::post('/payrolls/delete', 'ApiController@postTransfersPayrollsDelete');
	Route::post('/payrolls/add-payment', 'ApiController@postTransfersPayrollsAddPayment');
	Route::get('/payrolls/employees', 'ApiController@getTransfersPayrollsEmployees');
	Route::get('/payrolls/employee/details', 'ApiController@getTransfersPayrollsEmployeeDetails');
	Route::get('/payrolls/employee/paylist', 'ApiController@getTransfersPayrollsEmployeePayList');
	Route::get('/payrolls/employee/payments', 'ApiController@getTransfersPayrollsEmployeePayments');
	Route::get('/payrolls/employee/paydetail', 'ApiController@getTransfersPayrollsEmployeePayDetail');
	Route::post('/payrolls/employee/add', 'ApiController@postTransfersPayrollsEmployeeAdd');
	Route::post('/payrolls/employee/add-multiple', 'ApiController@postTransfersPayrollsEmployeeAddMultiple');
	Route::post('/payrolls/employee/update', 'ApiController@postTransfersPayrollsEmployeeUpdate');
	Route::post('/payrolls/employee/delete', 'ApiController@postTransfersPayrollsEmployeeDelete');
	Route::post('/payrolls/ejecutar', 'ApiController@postTransfersPayrollsExecute');
	Route::post('/payrolls/verificar', 'ApiController@postTransfersPayrollsVerify');
	Route::post('/payrolls/rechazar', 'ApiController@postTransfersPayrollsReject');

	// PROVEEDORES
	Route::get('/providers', 'ApiController@getTransfersProviders');
	Route::post('/providers/add', 'ApiController@postTransfersProvidersAdd');
	Route::post('/providers/delete', 'ApiController@postTransfersProvidersDelete');
	Route::get('/providers/accounts', 'ApiController@getTransfersProvidersAccounts');
	Route::post('/providers/add-payment', 'ApiController@postTransfersProvidersAddPayment');
	Route::get('/providers/history', 'ApiController@getTransfersProvidersHistory');
	Route::get('/providers/history/all', 'ApiController@getTransfersProvidersHistoryAll');
	Route::post('/providers/execute', 'ApiController@postTransfersProvidersExecute');
	Route::post('/providers/reject', 'ApiController@postTransfersProvidersReject');
	Route::post('/providers/verify', 'ApiController@postTransfersProvidersVerify');
	/* ---- EMPRESA ---- */
	Route::group(['prefix' => 'enterprise'], function(){
		// EJECUTAR, RECHAZAR, VERIFICAR
		Route::post('/execute', 'ApiController@postTransfersEnterpriseExecute');
		Route::post('/reject', 'ApiController@postTransfersEnterpriseReject');
		Route::post('/verify', 'ApiController@postTransfersEnterpriseVerify');

		// CUENTAS PROPIAS - EMPRESA
		Route::get('/own/accounts', 'ApiController@getTransfersEnterpriseOwnAccounts');
		Route::post('/own/transfer', 'ApiController@postTransfersEnterpriseOwnTransfer');
		Route::get('/transactions', 'ApiController@getTransfersEnterpriseTransactions');
		Route::get('/transaction-detail', 'ApiController@getTransfersEnterpriseTransactionDetail');

		// CUENTA DE TERCEROS - EMPRESA
		Route::get('/others/associated-accounts', 'ApiController@getTransfersEnterpriseOthersAssociatedAccounts');
		Route::get('/others/accounts', 'ApiController@getTransfersEnterpriseOthersAccounts');
		Route::post('/others/transfer', 'ApiController@postTransfersEnterpriseOthersTransfer');
		Route::post('/others/validate-account', 'ApiController@postTransfersEnterpriseOthersValidateAccount');
		Route::post('/others/add-account', 'ApiController@postTransfersEnterpriseOthersAddAccount');
		Route::post('/others/update-account', 'ApiController@postTransfersEnterpriseOthersUpdateAccount');
		Route::post('/others/delete-account', 'ApiController@postTransfersEnterpriseOthersDeleteAccount');

		// OTROS BANCOS - EMPRESA
		Route::get('/banks/available', 'ApiController@getTransfersEnterpriseBanksAvailable');
		Route::get('/banks/accounts', 'ApiController@getTransfersEnterpriseBanksAccounts');
		Route::get('/banks/associated-accounts', 'ApiController@getTransfersEnterpriseBanksAssociatedAccounts');
		Route::post('/banks/validate-account', 'ApiController@postTransfersEnterpriseBanksValidateAccount');
		Route::post('/banks/add-account', 'ApiController@postTransfersEnterpriseBanksAddAccount');
		Route::post('/banks/transfer', 'ApiController@postTransfersEnterpriseBanksTransfer');
		Route::post('/banks/update-account', 'ApiController@postTransfersEnterpriseBanksUpdateAccount');
		Route::post('/banks/delete-account', 'ApiController@postTransfersEnterpriseBanksDeleteAccount');

		// PAGOS DE PRESTAMOS
		Route::post('/loans/transfer', 'ApiController@postTransfersEnterpriseLoansTransfer');

		// DIVISAS
		Route::get('/exchange/history', 'ApiController@getTransfersEnterpriseExchangeHistory');
		Route::get('/exchange/accounts', 'ApiController@getTransfersEnterpriseExchangeAccounts');
		Route::post('/exchange/transfer', 'ApiController@postTransfersEnterpriseExchangeTransfer');
	});
	
});
/* ---------------------- */

/* ----- SERVICIOS ------ */
Route::group(['prefix' => 'services'], function(){
	// CLARO
	Route::post('/claro/balance', 'ApiController@postServicesClaroBalance');
	Route::post('/claro/payment', 'ApiController@postServicesClaroPayment');
	Route::post('/claro/superpacks/products', 'ApiController@postServicesClaroSuperPacksProducts');
	Route::post('/claro/superpacks/payment', 'ApiController@postServicesClaroSuperPacksPayment');
	Route::post('/claro/recarga/products', 'ApiController@postServicesClaroRecargaProducts');
	Route::post('/claro/recarga/payment', 'ApiController@postServicesClaroRecargaPayment');
	// TIGO
	Route::post('/tigo/balance', 'ApiController@postServicesTigoBalance');
	Route::post('/tigo/payment', 'ApiController@postServicesTigoPayment');
	Route::post('/tigo/paquetigos/products', 'ApiController@postServicesTigoPaquetigosProducts');
	Route::post('/tigo/paquetigos/payment', 'ApiController@postServicesTigoPaquetigosPayment');
	Route::post('/tigo/recarga/products', 'ApiController@postServicesTigoRecargaProducts');
	Route::post('/tigo/recarga/payment', 'ApiController@postServicesTigoRecargaPayment');
	Route::post('/tigo/tigostar/balance', 'ApiController@postServicesTigoTigoStarBalance');
	Route::post('/tigo/tigostar/payment', 'ApiController@postServicesTigoTigoStarPayment');
	// EEGSA
	Route::post('/eegsa/balance', 'ApiController@postServicesEegsaBalance');
	Route::post('/eegsa/payment', 'ApiController@postServicesEegsaPayment');
	Route::post('/eegsa/payment/reconnect', 'ApiController@postServicesEegsaPaymentReconnect');
	Route::post('/eegsa/bill', 'ApiController@postServicesEegsaBill');
	Route::post('/eegsa/bill/reconnect', 'ApiController@postServicesEegsaBillReconnect');

	// ENERGUATE
	Route::post('/energuate/balance', 'ApiController@postServicesEnerguateBalance');
	Route::post('/energuate/payment', 'ApiController@postServicesEnerguatePayment');
});
/* ---------------------- */

/* ----- MANAGEMENT ----- */
Route::group(['prefix' => 'management'], function(){
	Route::get('/lock-disclaimer', 'ApiController@getManagementLockDisclaimer');
	Route::post('/lock-account-balance', 'ApiController@postManagementLockAccountBalance');

	/* EMPRESA */
	Route::group(['prefix' => 'enterprise'], function(){

		// SOLICITUD DE CHEQUES
		Route::post('/checkbooks/request', 'ApiController@postManagementEnterpriseCheckbooksRequest');
		Route::get('/checkbooks/list', 'ApiController@getManagementEnterpriseCheckbooksList');
		Route::get('/checkbooks/detail', 'ApiController@getManagementEnterpriseCheckbooksDetail');
		Route::post('/checkbooks/execute', 'ApiController@postManagementEnterpriseCheckbooksExecute');
		Route::post('/checkbooks/verify', 'ApiController@postManagementEnterpriseCheckbooksVerify');
		Route::post('/checkbooks/reject', 'ApiController@postManagementEnterpriseCheckbooksReject');
	
		// SOLICITUD DE CHEQUES DE CAJA
		Route::post('/cashierchecks/request', 'ApiController@postManagementEnterpriseCashierchecksRequest');
		Route::get('/cashierchecks/list', 'ApiController@getManagementEnterpriseCashierchecksList');
		Route::get('/cashierchecks/detail', 'ApiController@getManagementEnterpriseCashierchecksDetail');
		Route::post('/cashierchecks/execute', 'ApiController@postManagementEnterpriseCashierchecksExecute');
		Route::post('/cashierchecks/verify', 'ApiController@postManagementEnterpriseCashierchecksVerify');
		Route::post('/cashierchecks/reject', 'ApiController@postManagementEnterpriseCashierchecksReject');

		// FACTORAJE
		Route::get('/factoring/lines', 'ApiController@getManagementEnterpriseFactoringLines');
		Route::get('/factoring/list', 'ApiController@getManagementEnterpriseFactoringList');
		Route::POST('/factoring/request', 'ApiController@postManagementEnterpriseFactoringRequest');
	});
});

/* ------ GENERAL ------ */
Route::group(['prefix' => 'general'], function(){
	// FAVORITOS
	Route::get('/favorites/loans', 'ApiController@getGeneralFavoritesLoans');
	Route::post('/favorites/loan', 'ApiController@postGeneralFavoritesLoan');
	Route::get('/favorites/services', 'ApiController@getGeneralFavoritesServices');
	Route::post('/favorites/service', 'ApiController@postGeneralFavoritesService');
	Route::get('/favorites/payrolls', 'ApiController@getGeneralFavoritesPayrolls');
	Route::post('/favorites/payroll', 'ApiController@postGeneralFavoritesPayroll');
	Route::get('/favorites/providers', 'ApiController@getGeneralFavoritesProviders');
	Route::post('/favorites/provider', 'ApiController@postGeneralFavoritesProvider');
});

